const async = require("async")
const Parser = require("binary-parser-encoder").Parser;
var fs = require('fs')
const Iconv = require('iconv').Iconv;
const ansi = require('./ansi.js')
const { lock, unlock } = require('os-lock');
function removezeros(buf) {
    var i = 0
    while (i < buf.length) {
        if (buf[i] === 0) {
            return buf.slice(0, i)
        }
        i++
    }

    return buf
}

async function SquishLock(fd, cb) {
    try {
        await lock(fd, 0, 1, {exclusive: true})
    } catch(error) {
        console.log(error.message)
    }
    cb()
}

async function SquishUnlockAndClose(fd, cb) {
    try {
        await unlock(fd, 0, 1)
    } catch(error) {
        console.log(error.message)
    }
    fs.close(fd)
    cb()
}

function SquishHash(str) {
    var hash = 0
    var g
    var b = Buffer.from(str.toString().toLowerCase())
    for (var i = 0; i < str.length; i++) {
        hash = (hash << 4) + b[i]
        g = hash & 0xf0000000
        if (g != 0) {
            hash |= g >> 24
            hash |= g
        }
    }

    return hash & 0x7fffffff
}

function SquishWriteMessage(file, msg, callback) {
    var inreplyto = msg.xmsg.reply_to
    fs.open(file + ".sqd", "r+", function(err, fd) {
        if (!err) {
            async.waterfall([(cb) => {
                SquishReadSqBaseHdr(file, (err, sqhdr) => {
                    cb(err, sqhdr)
                })
            }], (err, sqhdr) => {
                SquishLock(fd, () => {
                    var frame = sqhdr.free_frame
                    var sbuffer = Buffer.alloc(28)
                    const sqHdr = new Parser()
                    .endianess("little")
                    .uint32("id")
                    .uint32("next_frame")
                    .uint32("prev_frame")
                    .uint32("frame_length")
                    .uint32("msg_length")
                    .uint32("clen")
                    .uint16("frame_type")
                    .uint16("resv")

                    var found = false

                    async.whilst((cb) => {
                        cb(null, frame != 0 && !found)
                    },
                    (cb) => {
                        fs.read(fd, sbuffer, 0, 28, frame, (err, num) => {
                            if (!err) {
                                var f = sqHdr.parse(sbuffer)
                                if (f.frame_type === 1 && f.frame_length <= 238 + msg.msg_len + msg.ctrl_len) {
                                    found = true
                                    cb(null, f)
                                } else if (frame === sqhdr.last_free_frame) {
                                    found = true
                                    cb(null, f)
                                } else {
                                    cb(null, f)
                                }
                            } else {
                                cb(err, null)
                            }
                        })
                    },
                    (err, f) => {
                        if (!err) {
                            async.waterfall([(cbo) => {
                                if (typeof(f) !== "undefined" && f.frame_type === 1 && f.id === 0xAFAE4453) {
                                    var tbuffer = Buffer.alloc(28)
                                    async.waterfall([(cb) => {
                                        fs.read(fd, tbuffer, 0, 28, f.prev_frame, (err, num) => {
                                            if (!err) {
                                                var tf = sqHdr.parse(tbuffer)
                                                tf.next_frame = f.next_frame
                                                var tb = sqHdr.encode(tf)

                                                fs.write(fd, tb, 0, 28, f.prev_frame, (err, num) => {
                                                    if (!err) {
                                                        cb(null,f)
                                                    } else {
                                                        cb(err, null)
                                                    }
                                                })
                                            } else {
                                                cb(err, null)
                                            }
                                        })
                                    },
                                    (arg, cb) => {
                                        fs.read(fd, tbuffer, 0, 28, f.next_frame, (err, num) => {
                                            if (!err) {
                                                var tf = sqHdr.parse(tbuffer)
                                                tf.prev_frame = f.prev_frame
                                                var tb = sqHdr.encode(tf)
                                                fs.write(fd, tb, 0, 28, f.next_frame, (err, num) => {
                                                    if (!err) {
                                                        cb(null,f)
                                                    } else {
                                                        cb(err, null)
                                                    }
                                                })
                                            }
                                        })
                                    },
                                    (arg, cb) => {
                                        if (sqhdr.free_frame === frame) {
                                            sqhdr.free_frame = f.next_frame
                                        }
                                        if (sqhdr.last_free_frame === frame) {
                                            sqhdr.last_free_frame = f.prev_frame
                                        }
                                        f.clen = msg.ctrl.length
                                        f.msg_length = 238 + msg.msg.length + msg.ctrl.length
                                        f.next_frame = 0
                                        f.prev_frame = sqhdr.last_frame
                                        f.frame_length = f.msg_len
                                        f.frame_type = 0
                                        f.id = 0xAFAE4453
                                
                                        var fb = sqHdr.encode(f)

                                        msg.xmsg.umsgid = sqhdr.uid
                                        fs.write(fd, fb, 0, 28, frame, (err, num) =>{
                                            if (!err) {
                                                cb(null, f)
                                            } else {
                                                cb(err, null)
                                            }
                                        })
                                    },
                                    (arg, cb) => {
                                        const xMsg = new Parser()
                                        .endianess("little")
                                        .uint32("attr")
                                        .array("from", {
                                            type: "uint8",
                                            length: 36
                                        })
                                        .array("to", {
                                            type: "uint8",
                                            length: 36
                                        })
                                        .array("subject", {
                                            type: "uint8",
                                            length: 72
                                        })
                                        .array("orig_addr", {
                                            type: "uint16le",
                                            length: 4
                                        })
                                        .array("dest_addr", {
                                            type: "uint16le",
                                            length: 4
                                        })
                                        .array("date_written", {
                                            type: "uint16le",
                                            length: 2
                                        })
                                        .array("date_arrived", {
                                            type: "uint16le",
                                            length: 2
                                        })
                                        .int16("utc_ofs")
                                        .uint32("reply_to")
                                        .array("replies", {
                                            type: "uint32le",
                                            length: 9
                                        })
                                        .uint32("umsgid")
                                        .array("ftsc_date", {
                                            type: "uint8",
                                            length: 20
                                        })

                                        var xb = xMsg.encode(msg.xmsg)
                                        fs.write(fd, xb, 0, 238, frame + 28, (err, num) => {
                                            if (!err) {
                                                cb(null, f)
                                            } else {
                                                cb(err, null)
                                            }
                                        })
                                    }, (arg, cb) => {
                                        // write msg->ctrl
                                        fs.write(fd, msg.ctrl, 0, msg.ctrl.length, frame + 28 + 238, (err, num) => {
                                            if (!err) {
                                                cb(null, f)
                                            } else {
                                                cb(err, null)
                                            }
                                        })
                                    }, (arg, cb) => {
                                        // write msg->msg
                                        fs.write(fd, msg.msg, 0, msg.msg.length, frame + 28 + 238, (err, num) => {
                                            if (!err) {
                                                cb(null, f)
                                            } else {
                                                cb(err, null)
                                            }
                                        })                                        
                                    }, (arg, cb) => {
                                        // update sqhdr and write it
                                        sqhdr.uid++
                                        sqhdr.num_msg++
                                        sqhdr.high_msg = sqhdr.num_msg
                                        const sqBaseHeader = new Parser()
                                        .endianess("little")
                                        .uint16("len")
                                        .uint16("resv1")
                                        .uint32("num_msg")
                                        .uint32("high_msg")
                                        .uint32("skip_msg")
                                        .uint32("highwater")
                                        .uint32("uid")
                                        .array("base", {
                                            type: "uint8",
                                            length: 80
                                        })
                                        .uint32("begin_frame")
                                        .uint32("last_frame")
                                        .uint32("free_frame")
                                        .uint32("last_free_frame")
                                        .uint32("end_frame")
                                        .uint32("max_msgs")
                                        .uint16("keep_days")
                                        .uint16("sz_sqhdr")
                                        .array("resv2", {
                                            type: "uint8",
                                            length: 124
                                        })
                                        var sb = sqBaseHeader.encode(sqhdr)                               
                                        fs.write(fd, sb, 0, 256, 0, (err, num) => {
                                            if (!err) {
                                                cb(null, f)
                                            } else {
                                                cb(err, null)
                                            }
                                        })
                                    }], (err, val) => {
                                        cb(err, val)
                                    })
                                } else {
                                    // no free frame found, append the message
                                    var tbuffer = Buffer.alloc(28)
                                    frame = sqhdr.end_frame
                                    async.waterfall([(cb) => {
                                        if (sqhdr.last_frame != 0) {
                                            fs.read(fd, tbuffer, 0, 28, sqhdr.last_frame, (err, num) => {
                                                if (!err) {
                                                    var tf = sqHdr.parse(tbuffer)
                                                    tf.next_frame = frame
                                                    var tb = sqHdr.encode(tf)

                                                    fs.write(fd, tb, 0, 28, sqhdr.last_frame, (err, num) => {
                                                        if (!err) {
                                                            cb(null,frame)
                                                        } else {
                                                            cb(err, null)
                                                        }
                                                    })
                                                } else {
                                                    cb(err, null)
                                                }
                                            })
                                        } else {
                                            cb(null, frame)
                                        }
                                    },
                                    (arg, cb) => {
                                        var nm = {
                                            clen: msg.ctrl.length,
                                            msg_length: 238 + msg.msg.length + msg.ctrl.length,
                                            next_frame: 0,
                                            prev_frame: sqhdr.last_frame,
                                            frame_length: 238 + msg.msg.length + msg.ctrl.length,
                                            frame_type: 0,
                                            id: 0xAFAE4453,
                                        }
                                        msg.xmsg.umsgid = sqhdr.uid
                                            
                                        var fb = sqHdr.encode(nm)

                                        fs.write(fd, fb, 0, 28, frame, (err, num) =>{
                                            if (!err) {
                                                cb(null, nm)
                                            } else {
                                                cb(err, null)
                                            }
                                        })
                                    },
                                    (arg, cb) => {
                                        const xMsg = new Parser()
                                        .endianess("little")
                                        .uint32("attr")
                                        .array("from", {
                                            type: "uint8",
                                            length: 36
                                        })
                                        .array("to", {
                                            type: "uint8",
                                            length: 36
                                        })
                                        .array("subject", {
                                            type: "uint8",
                                            length: 72
                                        })
                                        .array("orig_addr", {
                                            type: "uint16le",
                                            length: 4
                                        })
                                        .array("dest_addr", {
                                            type: "uint16le",
                                            length: 4
                                        })
                                        .array("date_written", {
                                            type: "uint16le",
                                            length: 2
                                        })
                                        .array("date_arrived", {
                                            type: "uint16le",
                                            length: 2
                                        })
                                        .int16("utc_ofs")
                                        .uint32("reply_to")
                                        .array("replies", {
                                            type: "uint32le",
                                            length: 9
                                        })
                                        .uint32("umsgid")
                                        .array("ftsc_date", {
                                            type: "uint8",
                                            length: 20
                                        })

                                        var xb = xMsg.encode(msg.xmsg)
                                        fs.write(fd, xb, 0, 238, frame + 28, (err, num) => {
                                            if (!err) {
                                                cb(null, arg)
                                            } else {
                                                cb(err, null)
                                            }
                                        })
                                    }, (arg, cb) => {
                                        // write msg->ctrl
                                        fs.write(fd, msg.ctrl, 0, msg.ctrl.length, frame + 28 + 238, (err, num) => {
                                            if (!err) {
                                                cb(null, arg)
                                            } else {
                                                cb(err, null)
                                            }
                                        })
                                    }, (arg, cb) => {
                                        // write msg->msg
                                        fs.write(fd, msg.msg, 0, msg.msg.length, frame + 28 + 238 + msg.ctrl.length, (err, num) => {
                                            if (!err) {
                                                cb(null, arg)
                                            } else {
                                                cb(err, null)
                                            }
                                        })                                        
                                    }, (arg, cb) => {
                                        // update sqhdr and write it
                                        var sqh = {
                                            len: sqhdr.len,
                                            resv1: 0,
                                            num_msg: sqhdr.num_msg + 1,
                                            high_msg: sqhdr.num_msg + 1,
                                            skip_msg: sqhdr.skip_msg,
                                            highwater: sqhdr.highwater,
                                            uid: sqhdr.uid + 1,
                                            base: Buffer.from(("\0").padEnd(80, '\0')),
                                            begin_frame: sqhdr.begin_frame,
                                            last_frame: frame,
                                            free_frame: sqhdr.free_frame,
                                            last_free_frame: sqhdr.last_free_frame,
                                            end_frame: frame + arg.frame_length + 28,
                                            max_msgs: sqhdr.max_msgs,
                                            keep_days: sqhdr.keep_days,
                                            sz_sqhdr: sqhdr.sz_sqhdr,
                                            resv2: Buffer.from(("\0").padEnd(124, '\0'))
                                        }
                                        const sqBaseHeader = new Parser()
                                        .endianess("little")
                                        .uint16("len")
                                        .uint16("resv1")
                                        .uint32("num_msg")
                                        .uint32("high_msg")
                                        .uint32("skip_msg")
                                        .uint32("highwater")
                                        .uint32("uid")
                                        .array("base", {
                                            type: "uint8",
                                            length: 80
                                        })
                                        .uint32("begin_frame")
                                        .uint32("last_frame")
                                        .uint32("free_frame")
                                        .uint32("last_free_frame")
                                        .uint32("end_frame")
                                        .uint32("max_msgs")
                                        .uint16("keep_days")
                                        .uint16("sz_sqhdr")
                                        .array("resv2", {
                                            type: "uint8",
                                            length: 124
                                        })
                                        var sb = sqBaseHeader.encode(sqh)
                                        cb (null, sb)
                                    }, (sb, cb) => {         
                                        fs.write(fd, sb, 0, 256, 0, (err, num) => {
                                            if (!err) {
                                                cb(null, f)
                                            } else {
                                                cb(err, null)
                                            }
                                        })
                                    }], (err, val) => {
                                        cbo(err, val)
                                    })
                                }                                
                            }, (val, cb) => {
                                if (!err) {    
                                    // update idx
                                    const sqIndex = new Parser()
                                    .endianess("little")
                                    .uint32("ofs")
                                    .uint32("umsgid")
                                    .uint32("hash")
                                        
                                    var idx = {
                                        hash: SquishHash(msg.xmsg.to),
                                        ofs: frame,
                                        umsgid: msg.xmsg.umsgid
                                    }
                                    var ib = sqIndex.encode(idx)
                                    fs.open(file + ".sqi", "a", function(err, ifd) {
                                        if (!err) {
                                            fs.write(ifd, ib, (err, num) => {
                                                cb(err, null)
                                            })
                                        }
                                    })
                                } else {
                                    console.log(err.message)
                                    cb(err, null)
                                }
                            }, (val, cb) => {
                                if (inreplyto !== 0) {
                                    async.waterfall([(cb)=> {
                                        fs.open(file + ".sqi", "r", function(err, ifd) {
                                            if (!err) {
                                                var stop = false
                                                var x = 0
                                                var buffer = Buffer.alloc(12)
                                                const sqIndex = new Parser()
                                                .endianess("little")
                                                .uint32("ofs")
                                                .uint32("umsgid")
                                                .uint32("hash")
                                                async.whilst((cb) => {
                                                    cb(null, x < sqhdr.num_msg && !stop)
                                                }, (cb) => {
                                                    fs.read(ifd, buffer, 0, 12, x * 12, (err, num) => {
                                                        if (err || num != 12) {
                                                            if (err) {
                                                                console.log(err.message)
                                                            } else {
                                                                stop = true
                                                            }
                                                            cb(err, x)
                                                        } else {
                                                            var idx = sqIndex.parse(buffer)
                    
                                                            if (idx.umsgid === inreplyto) {
                                                                stop = true
                    
                                                                var sbuffer = Buffer.alloc(28)
                                                                const sqHdr = new Parser()
                                                                .endianess("little")
                                                                .uint32("id")
                                                                .uint32("next_frame")
                                                                .uint32("prev_frame")
                                                                .uint32("frame_length")
                                                                .uint32("msg_length")
                                                                .uint32("clen")
                                                                .uint16("frame_type")
                                                                .uint16("resv")
                                                                fs.read(fd, sbuffer, 0, 28, idx.ofs, (err, num) => {
                                                                    if (!err) {
                                                                        var sqh = sqHdr.parse(sbuffer)
                                                                        if (sqh.id != 0xAFAE4453 || sqh.frame_type != 0) {
                                                                            cb(null, x)
                                                                        } else {
                                                                            var frame = Buffer.alloc(sqh.frame_length)
                                                                            const xMsg = new Parser()
                                                                            .endianess("little")
                                                                            .uint32("attr")
                                                                            .array("from", {
                                                                                type: "uint8",
                                                                                length: 36
                                                                            })
                                                                            .array("to", {
                                                                                type: "uint8",
                                                                                length: 36
                                                                            })
                                                                            .array("subject", {
                                                                                type: "uint8",
                                                                                length: 72
                                                                            })
                                                                            .array("orig_addr", {
                                                                                type: "uint16le",
                                                                                length: 4
                                                                            })
                                                                            .array("dest_addr", {
                                                                                type: "uint16le",
                                                                                length: 4
                                                                            })
                                                                            .array("date_written", {
                                                                                type: "uint16le",
                                                                                length: 2
                                                                            })
                                                                            .array("date_arrived", {
                                                                                type: "uint16le",
                                                                                length: 2
                                                                            })
                                                                            .int16("utc_ofs")
                                                                            .uint32("reply_to")
                                                                            .array("replies", {
                                                                                type: "uint32le",
                                                                                length: 9
                                                                            })
                                                                            .uint32("umsgid")
                                                                            .array("ftsc_date", {
                                                                                type: "uint8",
                                                                                length: 20
                                                                            })
                                                                            fs.read(fd, frame, 0, sqh.frame_length, idx.ofs + 28, (err, num) => {
                                                                                if (!err) {
                                                                                    var xmsg = xMsg.parse(frame)
                                                                                    var rep = false
                                                                                    if (xmsg.replies[0] === 0) {
                                                                                        xmsg.replies[0] = inreplyto
                                                                                        rep = true
                                                                                    } else if (xmsg.replies[1] === 0) {
                                                                                        xmsg.replies[1] = inreplyto
                                                                                        rep = true
                                                                                    } else if (xmsg.replies[2] === 0) {
                                                                                        xmsg.replies[2] = inreplyto
                                                                                        rep = true
                                                                                    } else if (xmsg.replies[3] === 0) {
                                                                                        xmsg.replies[3] = inreplyto
                                                                                        rep = true
                                                                                    } else if (xmsg.replies[4] === 0) {
                                                                                        xmsg.replies[4] = inreplyto
                                                                                        rep = true
                                                                                    } else if (xmsg.replies[5] === 0) {
                                                                                        xmsg.replies[5] = inreplyto
                                                                                        rep = true
                                                                                    } else if (xmsg.replies[6] === 0) {
                                                                                        xmsg.replies[6] = inreplyto
                                                                                        rep = true
                                                                                    } else if (xmsg.replies[7] === 0) {
                                                                                        xmsg.replies[7] = inreplyto
                                                                                        rep = true
                                                                                    } else if (xmsg.replies[8] === 0) {
                                                                                        xmsg.replies[8] = inreplyto
                                                                                        rep = true
                                                                                    } 
                                                                                    if (rep == true) {
                                                                                        var xmsg2 = {
                                                                                            attr: xmsg.attribs,
                                                                                            from: xmsg.from,
                                                                                            to: xmsg.to,
                                                                                            subject: xmsg.subject,
                                                                                            orig_addr: xmsg.orig_addr,
                                                                                            dest_addr: xmsg.dest_addr,
                                                                                            utc_ofs: 0,
                                                                                            date_written: xmsg.date_written,
                                                                                            date_arrived: xmsg.date_arrived,
                                                                                            reply_to: xmsg.reply_to,
                                                                                            replies: xmsg.replies,
                                                                                            umsgid: xmsg.umsgid,
                                                                                            ftsc_date: xmsg.ftsc_date
                                                                                        }
                                                                                        var b = xMsg.encode(xmsg2)
                                                                                        fs.write(fd, b, 0, 238, idx.ofs + 28, (err, num) => {
                                                                                            cb(err, x);
                                                                                        })
                                                                                    } else {
                                                                                        cb(null, x)
                                                                                    }
                                                                                } else {
                                                                                    cb(err, 0)
                                                                                }
                                                                            })
                                                                        }
                                                                    }
                                                                });
                    
                                                            } else {
                                                                x++
                                                                cb(null, x)
                                                            }
                                                        }
                                                    })
                                                }, (err, num) => {
                                                    fs.close(ifd)
                                                    cb(err, msg)
                                                })
                                            }
                                        })
                                    }], (err, val) => {
                                        cb(err, null)
                                    })
                                } else {
                                    cb(null, null)
                                }
                            }], (err, val) => {
                                SquishUnlockAndClose(fd, callback)
                            })
                        } else {
                            SquishUnlockAndClose(fd, callback)
                        }
                    })
                })
            })
        }
    })
}

function SquishNextMessage(file, umsgid, cb) {
    SquishReadSqBaseHdr(file, (err, sqhdr) => {
        if (!err) {
            const sqIndex = new Parser()
            .endianess("little")
            .uint32("ofs")
            .uint32("umsgid")
            .uint32("hash")
            fs.open(file + ".sqi", "r", function(err, fd) {
                if (!err) {
                    var buffer = Buffer.alloc(12)
                    var x = 0;
                    var stop = false
                    var msg = null
                    var prev = -1
                    var next = -1
                    async.whilst((cb) => {
                        cb(null, x < sqhdr.num_msg && !stop)
                    }, (cb) => {
                        fs.read(fd, buffer, 0, 12, x * 12, (err, num) => {
                            if (err || num != 12) {
                                if (err) {
                                    console.log(err.message)
                                } else {
                                    stop = true
                                }
                                cb(err, x)
                            } else {
                                var idx = sqIndex.parse(buffer)
                                if (prev == umsgid) {
                                    next = idx.umsgid
                                    stop = true
                                } else {
                                    prev = idx.umsgid
                                }
                                x++
                                cb(err, x)
                            }
                        })
                    }, (err, num) => {
                        fs.close(fd)
                        cb(err, next)
                    })
                } else {
                    cb(err, -1)
                }
            })
        } else {
            cb(err, -1)
        }
    })
}

function SquishPrevMessage(file, umsgid, cb) {
    SquishReadSqBaseHdr(file, (err, sqhdr) => {
        if (!err) {
            const sqIndex = new Parser()
            .endianess("little")
            .uint32("ofs")
            .uint32("umsgid")
            .uint32("hash")
            fs.open(file + ".sqi", "r", function(err, fd) {
                if (!err) {
                    var buffer = Buffer.alloc(12)
                    var x = 0;
                    var stop = false
                    var msg = null
                    var prev = -1
                    async.whilst((cb) => {
                        cb(null, x < sqhdr.num_msg && !stop)
                    }, (cb) => {
                        fs.read(fd, buffer, 0, 12, x * 12, (err, num) => {
                            if (err || num != 12) {
                                if (err) {
                                    console.log(err.message)
                                } else {
                                    stop = true
                                }
                                cb(err, x)
                            } else {
                                var idx = sqIndex.parse(buffer)
                                if (idx.umsgid == umsgid) {
                                    stop = true
                                } else {
                                    prev = idx.umsgid
                                }
                                x++
                                cb(err, x)
                            }
                        })
                    }, (err, num) => {
                        fs.close(fd)
                        cb(err, prev)
                    })
                } else {
                    cb(err, -1)
                }
            })
        } else {
            cb(err, -1)
        }
    })
}

function SquishReadMessage(file, umsgid, raw, cb) {
    SquishReadSqBaseHdr(file, (err, sqhdr) => {
        var iconv = new Iconv("CP437", "UTF-8//TRANSLIT//IGNORE")
        if (!err) {
            const sqIndex = new Parser()
            .endianess("little")
            .uint32("ofs")
            .uint32("umsgid")
            .uint32("hash")

            fs.open(file + ".sqi", "r", function(err, fd) {
               if (!err) {
                fs.open(file + ".sqd", "r", (err, dfd) => {
                        if (!err) {
                            var buffer = Buffer.alloc(12)
                            var x = 0;
                            var stop = false
                            var msg = null
                            async.whilst((cb) => {
                                cb(null, x < sqhdr.num_msg && !stop)
                            }, (cb) => {
                                fs.read(fd, buffer, 0, 12, x * 12, (err, num) => {
                                    if (err || num != 12) {
                                        if (err) {
                                            console.log(err.message)
                                        } else {
                                            stop = true
                                        }
                                        cb(err, x)
                                    } else {
                                        var idx = sqIndex.parse(buffer)

                                        if (idx.umsgid === umsgid) {
                                            stop = true

                                            var sbuffer = Buffer.alloc(28)
                                            const sqHdr = new Parser()
                                            .endianess("little")
                                            .uint32("id")
                                            .uint32("next_frame")
                                            .uint32("prev_frame")
                                            .uint32("frame_length")
                                            .uint32("msg_length")
                                            .uint32("clen")
                                            .uint16("frame_type")
                                            .uint16("resv")
                                            fs.read(dfd, sbuffer, 0, 28, idx.ofs, (err, num) => {
                                                if (!err) {
                                                    var sqh = sqHdr.parse(sbuffer)
                                                    if (sqh.id != 0xAFAE4453 || sqh.frame_type != 0) {
                                                        cb(null, x)
                                                    } else {
                                                        var frame = Buffer.alloc(sqh.frame_length)
                                                        const xMsg = new Parser()
                                                        .endianess("little")
                                                        .uint32("attr")
                                                        .array("from", {
                                                            type: "uint8",
                                                            length: 36
                                                        })
                                                        .array("to", {
                                                            type: "uint8",
                                                            length: 36
                                                        })
                                                        .array("subject", {
                                                            type: "uint8",
                                                            length: 72
                                                        })
                                                        .array("orig_addr", {
                                                            type: "uint16le",
                                                            length: 4
                                                        })
                                                        .array("dest_addr", {
                                                            type: "uint16le",
                                                            length: 4
                                                        })
                                                        .array("date_written", {
                                                            type: "uint16le",
                                                            length: 2
                                                        })
                                                        .array("date_arrived", {
                                                            type: "uint16le",
                                                            length: 2
                                                        })
                                                        .int16("utc_ofs")
                                                        .uint32("reply_to")
                                                        .array("replies", {
                                                            type: "uint32le",
                                                            length: 9
                                                        })
                                                        .uint32("umsgid")
                                                        .array("ftsc_date", {
                                                            type: "uint8",
                                                            length: 20
                                                        })
                                                        fs.read(dfd, frame, 0, sqh.frame_length, idx.ofs + 28, (err, num) => {
                                                            if (!err) {
                                                                var xmsg = xMsg.parse(frame)

                                                                date = new Date()
                                                                date.setFullYear(((xmsg.date_written[0] >> 9) & 127) + 1980)
                                                                date.setMonth(((xmsg.date_written[0] >> 5) & 15) - 1)
                                                                date.setDate(xmsg.date_written[0] & 31)
                                                                date.setHours((xmsg.date_written[1] >> 11) & 31)
                                                                date.setMinutes((xmsg.date_written[1] >> 5) & 63)
                                                                date.setSeconds(xmsg.date_written[1] & 31)

    
    
                                                                var xmsgc = {
                                                                    id: xmsg.umsgid,
                                                                    from: iconv.convert(removezeros(Buffer.from(xmsg.from))).toString(),
                                                                    to: iconv.convert(removezeros(Buffer.from(xmsg.to))).toString(),
                                                                    subject: iconv.convert(removezeros(Buffer.from(xmsg.subject))).toString(),
                                                                    destination: (xmsg.dest_addr[0] == 0 ? xmsg.dest_addr[2].toString() : xmsg.dest_addr[0].toString() + ":" + xmsg.dest_addr[1].toString() + "/" + xmsg.dest_addr[2].toString() + "." + xmsg.dest_addr[3].toString()),
                                                                    origin: (xmsg.orig_addr[0] == 0 ? xmsg.orig_addr[2].toString() :xmsg.orig_addr[0].toString() + ":" + xmsg.orig_addr[1].toString() + "/" + xmsg.orig_addr[2].toString() + "." + xmsg.orig_addr[3].toString()),
                                                                    date: date
                                                                }
                                                                var ctrl = frame.subarray(238, 238 + sqh.clen)
                                                                var body = iconv.convert(frame.subarray(238 + sqh.clen, sqh.frame_length)).toString()

                                                                var lines = body.split("\r")
                                                                var gottearline = false
                                                                var newbody = ""

                                                                for (var j = 0; j < lines.length; j++) {
                                                                    if (!gottearline) {
                                                                        newbody += lines[j] + "\r"
                                                                        if (lines[j] === '---' || lines[j].substring(0, 4) === '--- ') {
                                                                            gottearline = true
                                                                        }
                                                                    } else {
                                                                        if (lines[j].substring(0, 1) != '\x01' && lines[j].substring(0, 8) != 'SEEN-BY:') {
                                                                            newbody += lines[j] + "\r"
                                                                        }
                                                                    }
                                                                }

                                                                if (!raw) {
                                                                    if (newbody.includes('\x1b')) {
                                                                        msg = {
                                                                            header: xmsgc,
                                                                            ctrl: ctrl,
                                                                            body: ansi.convert(newbody, 80)
                                                                        }
                                                                    } else {
                                                                        msg = {
                                                                            header: xmsgc,
                                                                            ctrl: ctrl,
                                                                            body: ansi.convert(newbody)
                                                                        }
                                                                    }
                                                                } else {
                                                                    msg = {
                                                                        header: xmsgc,
                                                                        ctrl: ctrl,
                                                                        body: newbody
                                                                    }
                                                                }
                                                                x++
                                                                cb(null, x)
                                                            } else {
                                                                cb(err, 0)
                                                            }
                                                        })
                                                    }
                                                }
                                            });

                                        } else {
                                            x++
                                            cb(null, x)
                                        }
                                    }
                                })
                            }, (err, num) => {
                                fs.close(dfd)
                                fs.close(fd)
                                cb(err, msg)
                            })
                        } else {
                            fs.close(dfd)
                            fs.close(fd)
                            cb(err, null)  
                        } 
                    })
                } else {
                    fs.close(fd)
                    cb(err, null)       
                }
            })
        }  
    })                
}

function SquishReadHeaders(file, skip, count, isnetmail, user, aka, realname, cb) {
    var total = 0
    SquishReadSqBaseHdr(file, (err, sqhdr) => {
        var iconv = new Iconv("CP437", "UTF-8//TRANSLIT//IGNORE")
        if (!err) {
            total = sqhdr.num_msg
            if (skip > sqhdr.num_msg) {
                cb(null, [], total)
            } else {
                const sqIndex = new Parser()
                .endianess("little")
                .uint32("ofs")
                .uint32("umsgid")
                .uint32("hash")
    
                fs.open(file + ".sqi", "r", function(err, fd) {
                    if (!err) {
                        fs.open(file + ".sqd", "r", (err, dfd) => {
                            if (!err) {
                                var buffer = Buffer.alloc(12)
                                var x = sqhdr.num_msg - 1;
                                var stop = false
                                var msgheaders = []
                                async.whilst((cb) => {
                                    cb(null, x - skip >= 0 && msgheaders.length < count && !stop)
                                }, (cb) => {
                                    fs.read(fd, buffer, 0, 12, (x - skip) * 12, (err, num) => {
                                        if (err || num != 12) {
                                            if (err) {
                                                console.log(err.message)
                                            } else {
                                                stop = true
                                            }
                                            cb(err, x)
                                        } else {
                                            var idx = sqIndex.parse(buffer)
                                            var sbuffer = Buffer.alloc(28)
                                            const sqHdr = new Parser()
                                            .endianess("little")
                                            .uint32("id")
                                            .uint32("next_frame")
                                            .uint32("prev_frame")
                                            .uint32("frame_length")
                                            .uint32("msg_length")
                                            .uint32("clen")
                                            .uint16("frame_type")
                                            .uint16("resv")

                                            fs.read(dfd, sbuffer, 0, 28, idx.ofs, (err, num) => {
                                                if (!err) {
                                                    var sqh = sqHdr.parse(sbuffer)
                                                    if (sqh.id != 0xAFAE4453 || sqh.frame_type != 0) {
                                                        cb(null, x)
                                                    } else {
                                                        var frame = Buffer.alloc(238)
                                                        const xMsg = new Parser()
                                                        .endianess("little")
                                                        .uint32("attr")
                                                        .array("from", {
                                                            type: "uint8",
                                                            length: 36
                                                        })
                                                        .array("to", {
                                                            type: "uint8",
                                                            length: 36
                                                        })
                                                        .array("subject", {
                                                            type: "uint8",
                                                            length: 72
                                                        })
                                                        .array("orig_addr", {
                                                            type: "uint16le",
                                                            length: 4
                                                        })
                                                        .array("dest_addr", {
                                                            type: "uint16le",
                                                            length: 4
                                                        })
                                                        .array("date_written", {
                                                            type: "uint16le",
                                                            length: 2
                                                        })
                                                        .array("date_arrived", {
                                                            type: "uint16le",
                                                            length: 2
                                                        })
                                                        .int16("utc_ofs")
                                                        .uint32("reply_to")
                                                        .array("replies", {
                                                            type: "uint32le",
                                                            length: 9
                                                        })
                                                        .uint32("umsgid")
                                                        .array("ftsc_date", {
                                                            type: "uint8",
                                                            length: 20
                                                        })
                                                        fs.read(dfd, frame, 0, 238, idx.ofs + 28, (err, num) => {
                                                            if (!err) {
                                                                var xmsg = xMsg.parse(frame)

                                                                date = new Date()
                                                                date.setFullYear(((xmsg.date_written[0] >> 9) & 127) + 1980)
                                                                date.setMonth(((xmsg.date_written[0] >> 5) & 15) - 1)
                                                                date.setDate(xmsg.date_written[0] & 31)
                                                                date.setHours((xmsg.date_written[1] >> 11) & 31)
                                                                date.setMinutes((xmsg.date_written[1] >> 5) & 63)
                                                                date.setSeconds(xmsg.date_written[1] & 31)



                                                                var xmsgc = {
                                                                    id: xmsg.umsgid,
                                                                    from: iconv.convert(removezeros(Buffer.from(xmsg.from))).toString(),
                                                                    to: iconv.convert(removezeros(Buffer.from(xmsg.to))).toString(),
                                                                    subject: iconv.convert(removezeros(Buffer.from(xmsg.subject))).toString(),
                                                                    destination: xmsg.dest_addr[0].toString() + ":" + xmsg.dest_addr[1].toString() + "/" + xmsg.dest_addr[2].toString() + "." + xmsg.dest_addr[3].toString(),
                                                                    origin: xmsg.orig_addr[0].toString() + ":" + xmsg.orig_addr[1].toString() + "/" + xmsg.orig_addr[2].toString() + "." + xmsg.orig_addr[3].toString(),
                                                                    date: date
                                                                }
                                                                if (isnetmail) {
                                                                    var added = false
                                                                    if (user.toUpperCase() === xmsgc.to.toUpperCase() ||
                                                                        realname.toUpperCase() === xmsgc.to.toUpperCase()) {

                                                                        if (!aka.includes(":")) {
                                                                            if (aka == xmsg.dest_addr[2].toString()) {
                                                                                msgheaders.push(xmsgc)
                                                                                added = true
                                                                            }
                                                                        } else {
                                                                            if (!aka.includes('.')) {
                                                                                aka = aka + ".0"
                                                                            }
                                                                            if (aka == xmsgc.destination) {
                                                                                msgheaders.push(xmsgc)
                                                                                added = true
                                                                            }
                                                                        }
                                                                    }
                                                                    if (!added) {
                                                                        if (user.toUpperCase() === xmsgc.from.toUpperCase() ||
                                                                            realname.toUpperCase() === xmsgc.from.toUpperCase()) {
                                                                            if (!aka.includes(':')) {
                                                                                if (aka == xmsg.orig_addr[2].toString()) {
                                                                                    msgheaders.push(xmsgc)
                                                                                }
                                                                            } else {                                                                            
                                                                                if (!aka.includes('.')) {
                                                                                    aka = aka + ".0"
                                                                                }
                                                                                if (aka == xmsgc.origin) {
                                                                                    msgheaders.push(xmsgc)
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                } else {
                                                                    msgheaders.push(xmsgc)
                                                                }
                                                                x--
                                                                cb(null, x)
                                                            } else {
                                                                stop = true
                                                                cb(err, x)
                                                            }
                                                        })
                                                    }

                                                }
                                            })
                                        }
                                    })
                                }, (err, num) => {
                                    if (err) {
                                        console.log(err.message)
                                    }
                                    fs.close(fd)
                                    fs.close(dfd)
                                    cb(err, msgheaders, total)
                                })
                            } else {
                                fs.close(fd)
                                cb(err, null, 0)
                            }
                        })
                    } else {
                        cb(err, null, 0)
                    }
                })
            }
        } else {
            cb(err, null, 0)
        }
    })
}

function SquishUMSGID2Offset(file, mid, nextm, cb)   {
    SquishReadSqBaseHdr(file, (err, sqhdr) => {
        if (!err) {
            const sqIndex = new Parser()
            .endianess("little")
            .uint32("ofs")
            .uint32("umsgid")
            .uint32("hash")

            fs.open(file + ".sqi", "r", function(err, fd) {
                if (!err) {
                    var buffer = Buffer.alloc(12)
                    var count = 0;
                    var stop = false;
                    async.whilst((cb) => {
                        cb(null, count < sqhdr.num_msg && stop == false)
                    }, (cb)=> {
                        fs.read(fd, buffer, 0, 12, count * 12, (err, num) => {
                            if (err || num != 12) {
                                stop = true;
                                if (err) {
                                    console.log(err.message)
                                } else {
                                    console.log(num)
                                }
                                cb(err, 0)
                            } else {
                                count++
                                var idx = sqIndex.parse(buffer)
                                if (idx.umsgid == mid)  {
                                    stop = true;
                                    cb(null, count)
                                } else
                                if (idx.umsgid > mid && nextm) {
                                    stop = true;
                                    cb(null, count)
                                } else {
                                    cb(null, count)
                                }
                                
                            }
                        })
                    }, (err, n) => {
                        fs.close(fd)
                        cb(err, count)
                    })
                } else {
                    cb(err, null)
                }
            })
        }
    })
}

function SquishReadSqBaseHdr(file, cb) {
    async.waterfall([(callback) => {
        const sqBaseHeader = new Parser()
        .endianess("little")
        .uint16("len")
        .uint16("resv1")
        .uint32("num_msg")
        .uint32("high_msg")
        .uint32("skip_msg")
        .uint32("highwater")
        .uint32("uid")
        .array("base", {
            type: "uint8",
            length: 80
        })
        .uint32("begin_frame")
        .uint32("last_frame")
        .uint32("free_frame")
        .uint32("last_free_frame")
        .uint32("end_frame")
        .uint32("max_msgs")
        .uint16("keep_days")
        .uint16("sz_sqhdr")
        .array("resv2", {
            type: "uint8",
            length: 124
        })

        fs.open(file + ".sqd", "r", function(err, fd) {
            if (err) {
                callback(err, null)
            } else {
                var buffer = Buffer.alloc(256)
                fs.read(fd, buffer, 0, 256, 0, function(err, num) {
                    if (err || num != 256) {
                        fs.close(fd)
                        callback(err, null)
                    } else {
                        fs.close(fd)
                        callback(null, sqBaseHeader.parse(buffer))
                    }
                })
            }
        })
    }], (err, res) => {
        if (err) {
            console.log(err.message)
            cb(err, null)
        } else {
            cb(null, res)
        }
    });
}

exports.SquishReadSqBaseHdr = SquishReadSqBaseHdr
exports.SquishUMSGID2Offset = SquishUMSGID2Offset
exports.SquishReadHeaders = SquishReadHeaders
exports.SquishReadMessage = SquishReadMessage
exports.SquishWriteMessage = SquishWriteMessage
exports.SquishNextMessage = SquishNextMessage
exports.SquishPrevMessage = SquishPrevMessage