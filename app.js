const express = require('express')
const app = express()
const port = 3000
const fs = require('fs');
const path = require('path');
const loadIniFile = require('read-ini-file')
const sqlite3 = require('sqlite3').verbose();
const md = require('markdown-it')({
html: true
})
const TOML = require('@iarna/toml')
const crypto = require('crypto');
const async = require('async');
const Iconv = require('iconv').Iconv;
const squish = require('./squish.js')
const ansi = require('./ansi.js')
var myArgs = process.argv.slice(2);

var talismanPath = myArgs[0]

const inifile = path.join(talismanPath, 'talisman.ini')
const talismanconfig = loadIniFile.sync(inifile)

function splitbody(bodystr) {
    var arr = bodystr.split("\r")
    var qbody = ""
    arr.forEach((element) => {
        qbody += " > " + element + "\n"
    })

    return qbody
}

const touch = (path, callback) => {
    const time = new Date();
    fs.utimes(path, time, time, err => {
      if (err) {
        return fs.open(path, 'w', (err, fd) => {
          err ? callback(err) : fs.close(fd, callback);
        });
      }
      callback();
    });
  };

var session = require('express-session');
var SQLiteStore = require('connect-sqlite3')(session);
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({
    store: new SQLiteStore,
    secret: talismanconfig.noddy['session secret'],
    cookie: { maxAge: 7 * 24 * 60 * 60 * 1000 }, // 1 week
    saveUninitialized: false,
    resave: false
}));

app.use(express.static('public'))

app.set('views', './views')
app.set('view engine', 'pug')

app.get('/', (req, res) => {
    if (fs.existsSync('content/index.md')) {
        const page = {
            title: `Home - ${talismanconfig.main['system name']}`,
            username: req.session.username,
            content: md.render(fs.readFileSync('content/index.md').toString())
        }
        res.render('page', page)
    } else {
        const page = {
            title: `Home - ${talismanconfig.main['system name']}`,
            username: req.session.username,
            content: md.render('## A new Talisman BBS\n\nThe sysop has not added an index page yet!\n')
        }
        res.render('page', page)        
    }
})

app.get('/usercheck', (req, res) => {
    var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/users.sqlite3', sqlite3.OPEN_READONLY, (err) => {
        if (err) {
            console.error(err.message);
            res.json({result: 'error'})
        } else {
            db.get("SELECT username FROM users WHERE username = ?", req.query.u, (err, row) => {
                if (err || !row) {
                    res.json({result: 'error'})
                } else {
                    res.json({result: 'success'})
                }
                db.close()
            })
        }
    });    
})

app.get('/oneliners', (req, res) => {
    fs.stat(talismanPath + '/' + talismanconfig.paths['script path'] + '/data/oneliners.dat', (err, stats) => {
        if (err) {
            console.error(err.message);
            var page = {
                title: `Oneliners - ${talismanconfig.main['system name']}`,
                username: req.session.username,
                error: 'No oneliners!'
            }
            res.render('error', page);
            return
        } else {
            var array = fs.readFileSync(talismanPath + '/' + talismanconfig.paths['script path'] + '/data/oneliners.dat').toString().split("\n");
            var oneliners = []

            array.forEach(a => {
                if (a.length > 31) {
                    var oneliner = {
                        author: a.substring(3, 19).trim(),
                        line: ansi.parse_pipe_codes(a.substring(31).trim())
                    }
                    oneliners.push(oneliner)
                }
            });

            var page = {
                title: `Oneliners - ${talismanconfig.main['system name']}`,
                username: req.session.username,
                data: oneliners
            }

            res.render('oneliners', page);
        }
    });
})

app.get('/last10', (req, res) => {
    var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/call_log.sqlite3', sqlite3.OPEN_READONLY, (err) => {
        if (err) {
            console.error(err.message);
            var page = {
                title: `Last 10 - ${talismanconfig.main['system name']}`,
                username: req.session.username,
                error: 'Unable to open database!'
            }
            res.render('error', page);
            return
        }
    });

    db.serialize(() => {
        db.all('SELECT id, username, node, timeon, timeoff, rundoor, upload, download, msgpost FROM calllog ORDER by id DESC LIMIT 10', (err, rows) => {
            if (err) {
                console.error(err.message);
                var page = {
                    title: `last 10 - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Unable to query database!'
                }
                res.render('error', page);
                return
            } else {
                var page = {
                    title: `last 10 - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    data: rows
                }
                res.render('last10', page);
            }
        });
    });

    db.close((err) => {
        if (err) {
            console.error(err.message);
        }
    });
});

app.get('/blog/:id', (req, res) => {
    var user = talismanconfig.noddy['news user'];
    var id = req.params['id']

    var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/gopher.sqlite3', sqlite3.OPEN_READONLY, (err) => {
        if (err) {
            console.error(err.message);
            var page = {
                title: `System News (Index) - ${talismanconfig.main['system name']}`,
                username: req.session.username,
                error: 'Unable to open database!'
            }
            res.render('error', page);
            return
        }
    });

    db.serialize(() => {
        db.get('SELECT subject, datestamp, body, author FROM phlog WHERE author = ? AND id = ?', [user, id], (err, row) => {
            if (err) {
                var page = {
                    title: `System News (Index) - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Error reading database'                   
                }
                res.render('error', page);
            } else {
                var page = {
                    title: `${row['subject']} - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    item: row
                }
                res.render('blogitem', page)
            }
        })
        db.close((err) => {
            if (err) {
                console.error(err.message);
            }
        })

    })
})

app.get('/download/:hash', (req, res) => {
    var hash = req.params['hash'];
    var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/noddy_downloads.sqlite3', sqlite3.OPEN_READWRITE, (err) => {
        if (err) {
            console.error(err.message);
            var page = {
                title: `download - ${talismanconfig.main['system name']}`,
                username: req.session.username,
                error: 'Unable to open database!'
            }
            res.render('error', page);
            return
        }
    });
    db.serialize(() => {
        db.get('SELECT hash, file, expiry FROM downloads WHERE hash = ?', hash, (err, row) => {
            if (err) {
                err.
                console.error(err.message);
                var page = {
                    title: `download - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: err.message
                }
                res.render('error', page);
                return
            } else {
                if (row) {
                    if (Math.floor(Date.now() / 1000) > row.expiry) {
                        var page = {
                            title: `download - ${talismanconfig.main['system name']}`,
                            username: req.session.username,
                            error: 'Link expired!'
                        }
                        res.render('error', page);
                        return                    
                    } else {
                        res.download(row.file);
                    }
                } else {
                    var page = {
                        title: `download - ${talismanconfig.main['system name']}`,
                        username: req.session.username,
                        error: "File unavailable."
                    }
                    res.render('error', page);    
                }
            }
        });

    	db.get('SELECT hash FROM downloads WHERE hash=?', hash, (err, row) => {
            fs.stat(talismanPath+'/temp/noddydl/'+hash, (err, stats) => {
                if (!err) {
	    	  fs.rmdirSync(talismanPath+'/temp/noddydl/'+hash, {recursive: true, force: true});
                }
	    });
	});
        db.run('DELETE FROM downloads WHERE hash = ?', hash);

	
	db.each('SELECT hash FROM downloads WHERE file LIKE ?', talismanPath+'/temp/noddydl/%', (err, row) => {
	     fs.rmSync(talismanPath+'/temp/noddydl/'+row.hash, {recursive: true, force: true});
	});
	

        db.run('DELETE FROM downloads WHERE expiry < ?', Math.floor(Date.now() / 1000));
    });

    db.close((err) => {
        if (err) {
            console.error(err.message);
        }
    });

})

app.get('/page/:file', (req, res) => {
    var file = req.params['file'];
    var dlfile = 'content/pages/' + file + '.md';

    if (!path.normalize(dlfile).startsWith('content/pages/') || dlfile.substring(dlfile.length - 3) != '.md') {
        res.status(403).render('403', {title: 'Access Denied!'});
    } else {
        if (fs.existsSync(dlfile)) {
            var page = {
                title: `${file.replace(/_/g," ")} - ${talismanconfig.main['system name']}`,
                username: req.session.username,
                content: md.render(fs.readFileSync(dlfile).toString())
            }

            res.render('page', page)
        } else {
            res.status(404).render('404', {title: 'Resource not found!', username: req.session.username});
        }

    }
})

app.get('/blog', (req, res) => {
    var user = talismanconfig.noddy['news user'];

    var skip = 0

    if (typeof req.query.skip !== 'undefined' && req.query.skip) {
        skip = req.query.skip
    }


    var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/gopher.sqlite3', sqlite3.OPEN_READONLY, (err) => {
        if (err) {
            console.error(err.message);
            var page = {
                title: `System News (Index) - ${talismanconfig.main['system name']}`,
                username: req.session.username,
                error: 'Unable to open database!'
            }
            res.render('error', page);
            return
        }
    });

    db.serialize(() => {
        db.all('SELECT id, subject, datestamp, body, author FROM phlog WHERE author = ? ORDER BY datestamp DESC LIMIT ?, 10', [user, skip], (err, rows) => {
            if (err) {
                var page = {
                    title: `System News (Index) - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Error reading database'                   
                }
                res.render('error', page);
            } else {
                var page = {
                    title: `System News (Index) - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    skip: skip,
                    items: rows
                }
                res.render('blog', page)
            }
        })
        db.close((err) => {
            if (err) {
                console.error(err.message);
            }
        })

    })
})

app.post('/login', (req, res) => {
    var user = req.body.user
    var pass = req.body.password

    req.session.regenerate((err) => {

        var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/users.sqlite3', sqlite3.OPEN_READONLY, (err) => {
            if (err) {
                console.error(err.message);
                var page = {
                    title: `Login - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Unable to open database!'
                }
                res.render('error', page);
                return
            }
        });

        db.serialize(() => {
            db.get('SELECT id, username, password, salt FROM users WHERE username = ?', user, (err, row) => {
                if (err) {
                    console.error(err.message);
                    var page = {
                        title: `Login - ${talismanconfig.main['system name']}`,
                        username: req.session.username,
                        error: 'Unable to query database!'
                    }
                    res.render('error', page);
                } else {
                    if (row) {
                        var sha256hash = crypto.createHash('sha256');
                        sha256hash.update(pass + row.salt);
                        var digest = sha256hash.digest();
                        const data = digest.toString('hex').toUpperCase();
                        if (data == row.password) {
                            req.session.uid = row.id;
                            req.session.username = row.username;
                            req.session.save(function (err) {
                                if (!err) {
                                    res.redirect('/');
                                } else {
                                    var page = {
                                        title: `Login - ${talismanconfig.main['system name']}`,
                                        username: req.session.username,
                                        error: 'Login failed!'
                                    }
                                    res.render('error', page);
                                }
                            })
                        } else {
                            var page = {
                                title: `Login - ${talismanconfig.main['system name']}`,
                                username: req.session.username,
                                error: 'Login failed!'
                            }
                            res.render('error', page);
                        }
                        
                    } else {
                        res.redirect("/login")
                    }
                }
            })
        });

        db.close((err) => {
            if (err) {
                console.error(err.message);
            }
        });
    })
})

app.get('/logout', (req,res) => {
    req.session.uid = null
    req.session.username = null
    req.session.save(function (err) {
        if (err) {
            var page = {
                title: `Logout - ${talismanconfig.main['system name']}`,
                username: req.session.username,
                error: 'Logout failed!'
            }
            res.render('error', page)
        } else {
            // regenerate the session, which is good practice to help
            // guard against forms of session fixation
            req.session.regenerate(function (err) {
                if (!err) {
                    res.redirect('/')
                } else {
                    var page = {
                        title: `Logout - ${talismanconfig.main['system name']}`,
                        username: req.session.username,
                        error: 'Logout failed!'
                    }
                    res.render('error', page);
                }
            })
        }
    })
})

app.get('/login', (req, res) => {
    var page = {
        title: `Login - ${talismanconfig.main['system name']}`,
        username: req.session.username,
    }

    res.render('login', page)
})

app.get('/files/*', (req, res, next) => {
    console.log('Request for file: ' + req.path)
    next()
}, (req, res) => {
    var file = path.normalize(req.path.substring(7));
    var dlfile = 'content/downloads/' + file;

    if (!path.normalize(dlfile).startsWith('content/downloads/') && !path.normalize(dlfile) == 'content/downloads') {
        res.status(403).render('403', {title: 'Access Denied!', username: req.session.username});
    } else {
        if (fs.existsSync(dlfile)) {
            if (fs.lstatSync(dlfile).isDirectory()) {

                var thefiles = []

                fs.readdirSync(dlfile).forEach(a => {
                    if (!a.startsWith('.')) {
                        thefiles.push(a);
                    }
                });

                var page = {
                    title: `Contents of /files/${file} - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    path: file,
                    files: thefiles
                }


                res.render('files', page)
            } else {
                if (!file.startsWith('.')) {
                    res.download(dlfile);
                } else {
                    res.status(403).render('403', {title: 'Access Denied!', username: req.session.username});
                }
            }
        } else {
            res.status(404).render('404', {title: 'Resource not found!', username: req.session.username});
        }
    }
});

app.get('/bulletins/:hotkey', (req, res) => {
    if (req.session.username) {
        var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/users.sqlite3', sqlite3.OPEN_READWRITE, (err) => {
            if (err) {
                console.log(err.message);
                var page = {
                    title: `Bulletins - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Unable to open database!'
                }
                res.render('error', page);
                return
            }
        });
        db.serialize(() => {
            db.get("SELECT value FROM details WHERE attrib='seclevel' AND uid = ?", req.session.uid, (err, row) => {
                if (!err && row) {
                    var seclevel = parseInt(row['value'], 10)
                    var bulls = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/bulletins.toml'))

                    var tot = 0
                    
                    var i = bulls['bulletin'].findIndex((a, index, array) => {
			return (a['hotkey'] == req.params.hotkey);
		    });
    
                    if (i != -1) {
			b = bulls['bulletin'][i];
		        if (b['sec_level'] <= seclevel) {
                            var iconv = new Iconv("CP437", "UTF-8//TRANSLIT//IGNORE")
                            if (fs.existsSync(talismanPath + '/' + talismanconfig.paths['gfile path'] + '/' + b['file'] + '.ans')) {
                                var data = fs.readFileSync(talismanPath + '/' + talismanconfig.paths['gfile path'] + '/' + b['file'] + '.ans')

                                // remove SAUCE if it exists
                                var len = data.length;
                                if (data.indexOf(0x1a) != -1) {
                                    len = data.length - (data.length - data.indexOf(0x1a));
                                }
                                var buff = Buffer.alloc(len)
                                data.copy(buff, 0, 0, len)
                                            
                                // Convert to UTF-8
                                var str = iconv.convert(buff).toString()
                                // render ansi codes.
                                var out =  ansi.convert(str)

                                var page = {
                                    title: b['name'],
                                    username: req.session.username,
                                    content: out
                                }
                                res.render('bulletin', page)
                            } else if (fs.existsSync(talismanPath + '/' + talismanconfig.paths['gfile path'] + '/' + b['file'] + '.asc')) {
                                var data = fs.readFileSync(talismanPath + '/' + talismanconfig.paths['gfile path'] + '/' + b['file'] + '.asc')
                                    
                                // remove SAUCE if it exists
                                var len = data.length;
                                if (data.indexOf(0x1a) != -1) {
                                    len = data.length - (data.length - data.indexOf(0x1a));
                                }                    
                                var buff = Buffer.alloc(len)
                                data.copy(buff, 0, 0, len)
                    
                                // Convert to UTF-8
                     
                                var str = iconv.convert(buff).toString()
                                var out = ansi.convert(str)
                    
                                var page = {
                                    title: b['name'],
                                    username: req.session.username,
                                    content: out
                                }
                                res.render('bulletin', page)
                            } else {
                                res.status(404).render('404', {title: 'Resource not found!', username: req.session.username});
                            }
                        } else {
                            res.status(403).render('403', {title: 'Access Denied!', username: req.session.username});
                            return
                        }
                    } else {
                        res.status(404).render('404', {title: 'Resource not found!', username: req.session.username});
                    }
                    db.close()
                } else {
                    var page = {
                        title: `Bulletins - ${talismanconfig.main['system name']}`,
                        username: req.session.username,
                        error: 'An error occured'
                    }
                    res.render('error', page);
                    db.close()
                }
            })
        })
    } else {
        res.redirect("/login")
    }
});

app.get('/email', (req, res) => {
    if (req.session.username) {
        var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/email.sqlite3', sqlite3.OPEN_READONLY, (err) => {
            if (err) {
                console.error(err.message);
                var page = {
                    title: `Email - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Unable to open database!'
                }
                res.render('error', page);
                return
            }
        });

        db.serialize(() => {
            db.all('SELECT id, sender, subject, date, seen FROM email WHERE recipient = ? ORDER BY date DESC', req.session.username, (err, rows) => {
                if (err) {
                    console.error(err.message);
                    var page = {
                        title: `Email - ${talismanconfig.main['system name']}`,
                        username: req.session.username,
                        error: 'Unable to query database!'
                    }
                    res.render('error', page);
                    return
                } else {
                    var page = {
                        title: `Email - ${talismanconfig.main['system name']}`,
                        username: req.session.username,
                        data: rows
                    }
                    res.render('email', page);
                }
            });
        });

        db.close((err) => {
            if (err) {
                console.error(err.message);
            }
        });
    } else {
        res.redirect("/login")
    }
})

app.get('/email/new', (req, res) => {
    if (req.session.username) {
        var page = {
            title: `New Email - ${talismanconfig.main['system name']}`,
            username: req.session.username,
        }
        res.render('emailnew', page);
    } else {
        res.redirect("/login")
    }
})

app.get('/email/delete/:eid', (req, res) => {
    if (req.session.username) {
        var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/email.sqlite3', sqlite3.OPEN_READWRITE, (err) => {
            if (err) {
                console.error(err.message);
                var page = {
                    title: `Email - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Unable to open database!'
                }
                res.render('error', page);
                return
            }
        });
        db.serialize(() => {
            db.run("DELETE FROM email WHERE id = ? AND recipient = ?", req.params.eid, req.session.username, (err) => {
                if (err) {
                    console.error(err.message);
                }
            })
        });
        db.close((err) => {
            if (err) {
                console.error(err.message);
            }
        });       
        res.redirect("/email")
    } else {
        res.redirect("/login")
    }
})

app.get('/email/reply/:eid', (req, res) => {
    if (req.session.username) {
        var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/email.sqlite3', sqlite3.OPEN_READWRITE, (err) => {
            if (err) {
                console.error(err.message);
                var page = {
                    title: `Email - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Unable to open database!'
                }
                res.render('error', page);
                return
            }
        });
        db.serialize(() => {
            db.get('SELECT sender, subject, body FROM email WHERE recipient = ? AND id = ?', req.session.username, req.params.eid, (err, row) => {
                if (err) {
                    console.error(err.message);
                    var page = {
                        title: `Email - ${talismanconfig.main['system name']}`,
                        username: req.session.username,
                        error: 'Unable to query database!'
                    }
                    res.render('error', page);
                } else if (row) {
                    var page = {
                        title: `Email - ${talismanconfig.main['system name']}`,
                        username: req.session.username,
                        sender: row['sender'],
                        subject: "Re: " + row['subject'],
                        quote: splitbody(row['body'].toString())
                    }
                    res.render('emailnew', page);
                } else {
                    var page = {
                        title: `Email - ${talismanconfig.main['system name']}`,
                        username: req.session.username,
                        error: 'No such email!'
                    }
                    res.render('error', page);                   
                }
            })
        });
        
        db.close((err) => {
            if (err) {
                console.error(err.message);
            }
        });
    } else {
        res.redirect("/login")
    }
})

app.post('/email', (req, res) => {

    if (req.session.username) {
        async.series ([(callback) => {
            var recipient = ""
            var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/users.sqlite3', sqlite3.OPEN_READONLY, (err) => {
                if (err) {
                    console.error(err.message);
                    var page = {
                        title: `Email - ${talismanconfig.main['system name']}`,
                        username: req.session.username,
                        error: 'Unable to open database!'
                    }
                    res.render('error', page);
                    return
                }
            });
            db.serialize(() => {
                db.get("SELECT username FROM users WHERE username = ?", req.body.recipient, (err, row) => {
                    if (err) {
                        console.error(err.message);
                        var page = {
                            title: `Email - ${talismanconfig.main['system name']}`,
                            username: req.session.username,
                            error: 'Unable to query database!'
                        }
                        res.render('error', page)
                    } else if (row) {
                        recipient = row['username']
                        db.close((err) => {
                            if (err) {
                                console.error(err.message);
                            }
                        });            
                        callback(null, recipient)
                    } else {
                        db.get("SELECT uid FROM details WHERE attrib='fullname' AND value=?", req.body.recipient, (err, row) => {
                            if (err) {
                                console.error(err.message);
                                var page = {
                                    title: `Email - ${talismanconfig.main['system name']}`,
                                    username: req.session.username,
                                    error: 'Unable to query database!'
                                }
                                res.render('error', page)
                            } else if (row) {
                                db.get("SELECT username FROM users WHERE id = ?", row['uid'], (err, row) => {
                                    if (err) {
                                        console.error(err.message);
                                        var page = {
                                            title: `Email - ${talismanconfig.main['system name']}`,
                                            username: req.session.username,
                                            error: 'Unable to query database!'
                                        }
                                        res.render('error', page)    
                                    } else if (row) {
                                        recipient = row['username']
                                        db.close((err) => {
                                            if (err) {
                                                console.error(err.message);
                                            }
                                        })
                                        callback(null, recipient)
                                    } 
                                })
                            } else {
                                db.close((err) => {
                                    if (err) {
                                        console.error(err.message);
                                    }
                                });
                                callback(null, recipient)
                            }
                        })
                    }
                })         
            });
        }], (err, results) => {
            if (results[0] != "") {
                var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/email.sqlite3', sqlite3.OPEN_READWRITE, (err) => {
                    if (err) {
                        console.error(err.message);
                        var page = {
                            title: `Email - ${talismanconfig.main['system name']}`,
                            username: req.session.username,
                            error: 'Unable to open database!'
                        }
                        res.render('error', page);
                        return
                    }
                });
                db.serialize(() => {
                    var iconv = new Iconv("UTF-8", "CP437//TRANSLIT//IGNORE")
                    var b = iconv.convert(req.body.body.replace(/\r\n/g, "\r").toString() + "\r")
                    db.run("INSERT INTO email (sender, recipient, subject, body, date, seen) VALUES(?, ?, ?, ?, ?, 0)", req.session.username, results[0], req.body.subject, b, Math.floor(Date.now() / 1000), (err) => {
                        if (err) {
                            console.error(err.message);
                            var page = {
                                title: `Email - ${talismanconfig.main['system name']}`,
                                username: req.session.username,
                                error: 'Unable to send email!'
                            }
                            res.render('error', page);
                            return
                        } else {
                            res.redirect("/email")
                        }
                    })
                });
                db.close((err) => {
                    if (err) {
                        console.error(err.message);
                    }
                });
            } else {
                var page = {
                    title: `Email - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'No such recipient'
                }
                res.render('error', page)   
            }
        });
        
    } else {
        res.redirect("/login")
    }
})

app.get('/email/:eid', (req, res) => {
    if (req.session.username) {
        var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/email.sqlite3', sqlite3.OPEN_READWRITE, (err) => {
            if (err) {
                console.error(err.message);
                var page = {
                    title: `Email - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Unable to open database!'
                }
                res.render('error', page);
                return
            }
        });
        db.serialize(() => {
            db.run('UPDATE email SET seen = 1 WHERE id = ? AND recipient = ?', req.params.eid, req.session.username, (err) => {
                if (err) {
                    console.log("Error " + err)
                }
            })
            db.get('SELECT id, sender, subject, body, date, seen FROM email WHERE recipient = ? AND id = ?', req.session.username, req.params.eid, (err, row) => {
                if (err) {
                    console.error(err.message);
                    var page = {
                        title: `Email - ${talismanconfig.main['system name']}`,
                        username: req.session.username,
                        error: 'Unable to query database!'
                    }
                    res.render('error', page);
                } else if (row) {
                    var page = {
                        title: `Email - ${talismanconfig.main['system name']}`,
                        username: req.session.username,
                        data: row
                    }
                    res.render('emailitem', page);
                } else {
                    var page = {
                        title: `Email - ${talismanconfig.main['system name']}`,
                        username: req.session.username,
                        error: 'No such email!'
                    }
                    res.render('error', page);                   
                }
            })
        });
    
        db.close((err) => {
            if (err) {
                console.error(err.message);
            }
        });
    } else {
        res.redirect("/login")
    }
})

app.get('/bulletins', (req, res) => {
    if (req.session.username) {
        if (req.session.username) {
            var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/users.sqlite3', sqlite3.OPEN_READWRITE, (err) => {
                if (err) {
                    console.log(err.message);
                    var page = {
                        title: `Bulletins - ${talismanconfig.main['system name']}`,
                        username: req.session.username,
                        error: 'Unable to open database!'
                    }
                    res.render('error', page);
                    return
                }
            });
            db.serialize(() => {
                db.get("SELECT value FROM details WHERE attrib='seclevel' AND uid = ?", req.session.uid, (err, row) => {
                    if (!err && row) {
                        var seclevel = parseInt(row['value'], 10)
                        var bulls = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/bulletins.toml'))
                        var items = []
                        bulls['bulletin'].forEach(a => {
                            var item = {
                                file: a['file'],
                                title: a['name'],
                                hotkey: a['hotkey']
                            }

                            if (seclevel >= a['sec_level']) {
                                items.push(item)
                            }
                        })
                        var page = {
                            title: `Bulletins - ${talismanconfig.main['system name']}`,
                            username: req.session.username,
                            items: items
                        }
                        res.render('bulletins', page)
                        db.close()
                    } else {
                        var page = {
                            title: `Bulletins - ${talismanconfig.main['system name']}`,
                            username: req.session.username,
                            error: 'An error occured!'
                        }
                        res.render('error', page);
                        db.close()
                    }
                })
            })
        }
    } else {
        res.redirect('/login')
    }
});

function parse_fido_addr(str) {
    var arr = str.split(/\:|\/|\./)

    if (arr.length === 3) {
        return [parseInt(arr[0], 10), parseInt(arr[1], 10), parseInt(arr[2], 10), 0]
    } else if (arr.length === 4) {
        return [parseInt(arr[0], 10), parseInt(arr[1], 10), parseInt(arr[2], 10), parseInt(arr[3], 10)]
    }
}

function isnetmailarea(netmail) {
    if (netmail == null) {
        return false
    } else {
        return netmail
    }
}

function getmsgid(cb) {
    async.waterfall([(cb) => {
        fs.open(talismanPath + "/" + talismanconfig.paths['data path'] + '/msgserial.dat', 'r', function(err, fd) {
            if (!err) {
                var b = Buffer.alloc(4)
                fs.read(fd, b, 0, 4, 0, function(err, num) {
                    if (!err && num == 4) {
                        b.readUInt32LE(0)
                        fs.close(fd)
                        cb (null, b.readUInt32LE(0))
                    } else {
                        fs.close(fd)
                        cb(null, 0)
                    }
                })
            } else {
                cb(null, 0)
            }
        })
    }, (msgid, cb) => {
        var unixtimestamp = Math.floor(Date.now() / 1000)

        if (unixtimestamp > msgid) {
            msgid = unixtimestamp
        } else {
            msgid++
        }
        fs.open(talismanPath + "/" + talismanconfig.paths['data path'] + '/msgserial.dat', 'w', function(err, fd) {
            if (!err) {
                var b = Buffer.alloc(4)
                b.writeUint32LE(msgid, 0)
                fs.write(fd, b, 0, 4, 0, function(err, n, b) {
                    fs.close(fd)
                    cb(null, msgid)
                })
            } else {
                fs.close(fd)
                cb(null, msgid)
            }
        })
    }], (err, msgid) => {
        var hex = msgid.toString(16).toUpperCase()
        cb(hex)
    })
}

function get_replyid(file, umsgid, cb) {
    squish.SquishReadMessage(file, umsgid, true, (err, msg) => {
        var found = false
        if (!err) {
            var arr = msg.ctrl.toString().split("\x01")

            arr.forEach((element) => {
                if (element.substring(0, 6) === "MSGID:") {
                    cb("\x01REPLY: " + element.substring(7))
                    found = true
                }
            })
        } 
        if (!found) {
            cb("")
        }
    })
}

app.post('/msgbase/post', (req, res) => {
    var iconv = new Iconv("UTF-8", "CP437//TRANSLIT//IGNORE")
    if (req.session.username) {
        var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/users.sqlite3', sqlite3.OPEN_READWRITE, (err) => {
            if (err) {
                console.log(err.message);
                var page = {
                    title: `Post Message - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Unable to open database!'
                }
                res.render('error', page);
                return
            }
        });
        db.serialize(() => {
            db.get("SELECT value FROM details WHERE attrib='seclevel' AND uid = ?", req.session.uid, (err, row) => {
                if (!err && row) {
                    var seclevel = parseInt(row['value'], 10)
                    var msgconfs = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/msgconfs.toml'))
                    var carr = msgconfs['messageconf']
                    var last_read = 0
                    if (req.body.conf <= carr.length && req.body.conf > 0) {
                        
                        if (carr[req.body.conf -1]['sec_level'] <= seclevel) {

                            var msgareas = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/' + carr[req.body.conf-1]['config'] + ".toml"))
                            var aarr = msgareas['messagearea']
                            if (req.body.area <= aarr.length && req.body.area > 0) {
                                if (aarr[req.body.area-1]['write_sec_level'] <= seclevel) {

                                    db.get("SELECT value FROM details WHERE attrib='fullname' AND uid = ?", req.session.uid, (err, row) => {
                                        if (!err && row) {
                                            getmsgid((msgid) => {
                                                squish.SquishReadSqBaseHdr(talismanPath + '/' + talismanconfig.paths['message path'] + '/' + aarr[req.body.area-1]['file'], (err, sqh) => {
                                                    if (!err) {
                                                        var mailtype = 0
                                                        var reply = 0
                                                        async.waterfall([(cb) => {
                                                            if (typeof(req.body.reply) !== "undefined") {
                                                                reply = parseInt(req.body.reply, 10)
                                                            }

                                                            if (reply !== 0) {
                                                                get_replyid(talismanPath + '/' + talismanconfig.paths['message path'] + '/' + aarr[req.body.area-1]['file'], reply, (str) => {
                                                                    cb(null, str)
                                                                })
                                                            } else {
                                                                cb(null, "")
                                                            }
                                                        }, (replyk, cb) => {
                                                            require('child_process').exec('git rev-parse --short HEAD', function(err, stdout) {
                                                                var gitrev = "Unknown"
                                                                if (!err) {
                                                                    gitrev = stdout.replace(/\n/g, "")
                                                                }
                                                                var from = req.session.username
                                                                var daka = [0,0,0,0]
                                                                if (aarr[req.body.area - 1]['real_names'] === true) {
                                                                    from = row['value']
                                                                }

                                                                var jsdate = new Date()
                                                                var sqdate = 0

                                                                sqdate |= jsdate.getDate() & 31
                                                                sqdate |= ((jsdate.getMonth() + 1) & 15) << 5
                                                                sqdate |= ((jsdate.getFullYear() - 1980) & 127) << 9

                                                                var sqtime

                                                                sqtime |= jsdate.getSeconds() & 31
                                                                sqtime |= (jsdate.getMinutes() & 63) << 5
                                                                sqtime |= (jsdate.getHours() & 31) << 11

                                                                var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

                                                                var ftscdate = jsdate.getDate().toString().padStart(2,0) + ' ' + months[jsdate.getMonth()] + ' ' + (jsdate.getFullYear() - 2000).toString().padStart(2, 0) + '  ' + jsdate.getHours().toString().padStart(2,0) + ':' + jsdate.getMinutes().toString().padStart(2, 0) + ':' + jsdate.getSeconds().toString().padStart(2, 0)

                                                                var aka = []
                                                                var origin = ""
                                                                var ctrl = ""
                                                                var tzoff = jsdate.getTimezoneOffset()
                                                                var attribs = 0x100 | 0x20000 // MGS_LOCAL | MSG_UID (TODO: set MSG_PRIVATE for Netmail)
                                                                ctrl += "\x01CHRS: CP437 2"

                                                                ctrl += "\x01TZUTC: "

                                                                if (tzoff > 0) {
                                                                    ctrl += "-"
                                                                }

                                                                tzoff = Math.abs(tzoff)
                                                                if (tzoff / 60 < 10) {
                                                                    ctrl += '0'
                                                                }
                                                                ctrl += (Math.floor(tzoff / 60)).toString()

                                                                if (tzoff % 60 < 10) {
                                                                    ctrl += '0'
                                                                }

                                                                ctrl += (Math.floor(tzoff % 60)).toString()

                                                                if (typeof(aarr[req.body.area-1]['aka']) !== "undefined") {
                                                                    mailtype = 2
                                                                    aka = parse_fido_addr(aarr[req.body.area-1]['aka'])
                                                                    if  (typeof(aarr[req.body.area-1]['netmail']) !== "undefined" && aarr[req.body.area-1]['netmail'] == true && typeof(req.body.dest) !== "undefined") {
                                                                        daka = parse_fido_addr(req.body.dest)
                                                                        attribs |= 0x1
                                                                        mailtype = 1
                                                                        ctrl += "\x01INTL " + daka[0].toString() + ":" + daka[1].toString() + "/" + daka[2].toString() + " " + aka[0].toString() + ":" + aka[1].toString() + "/" + aka[2].toString()
                                                                        if (aka[3] > 0) {
                                                                            ctrl += "\x01FMPT " + aka[3].toString()
                                                                        }
                                                                        if (daka[3] > 0) {
                                                                            ctrl += "\x01TOPT " + daka[3].toString()
                                                                        }
                                                                    }
                                                                    ctrl += "\x01MSGID: " + aarr[req.body.area-1]['aka'] + " " + msgid
                                                                    origin = "--- Noddy git-" + gitrev + "\r * Origin: "
                                                                    
                                                                    origin += carr[req.body.conf-1]['tagline']
                                                                    origin += " (" + aka[0].toString() + ":" + aka[1].toString() + "/" + aka[2].toString()
                                                                    if (aka[3] > 0) {
                                                                        origin += "." + aka[3].toString()
                                                                    }
                                                                    origin += ")\r"
                                                                } else if (typeof(aarr[req.body.area-1]['wwivnode']) !== "undefined") {
                                                                    mailtype = 2
                                                                    ctrl += "\x01MSGID: " + "20000:20000/" + aarr[req.body.area-1]['wwivnode'] + " " + msgid
                                                                    if (typeof(aarr[req.body.area-1]['netmail']) !== "undefined" && aarr[req.body.area-1]['netmail'] == true && typeof(req.body.dest) !== "undefined") {
                                                                        daka = [20000, 20000,parseInt(req.body.dest, 10), 0]
                                                                        attribs |= 0x1
                                                                        mailtype = 1
                                                                    }

                                                                    aka = [20000, 20000, parseInt(aarr[req.body.area-1]['wwivnode'], 10), 0]
                                                                    origin = "--- Noddy git-" + gitrev + "\r * Origin: "
                                                                    origin += carr[req.body.conf-1]['tagline']
                                                                    origin += " (" + aarr[req.body.area-1]['wwivnode'] + ")\r"
                                                                } else {
                                                                    aka = [0,0,0,0]
                                                                    var sfile = aarr[req.body.area-1]['file'].replace(/\//g, "_").replace(/\\\\/g, "_")

                                                                    ctrl += "\x01MSGID: <" + msgid + "." + sqh.uid + "." + sfile + "@" + talismanconfig.main['hostname'] + ">"

                                                                }                                                            

                                                                ctrl += replyk

                                                                var xmsg = {
                                                                    attr: attribs,
                                                                    from: Buffer.from(from.slice(0, 36).padEnd(36, '\0'), 0, 36),
                                                                    to: Buffer.from(iconv.convert(req.body.recipient).toString().slice(0, 36).padEnd(36, '\0'), 0, 36),
                                                                    subject: Buffer.from(iconv.convert(req.body.subject).toString().slice(0, 72).padEnd(72, '\0'), 0, 72),
                                                                    orig_addr: aka,
                                                                    dest_addr: daka,
                                                                    utc_ofs: 0,
                                                                    date_written: [sqdate, sqtime],
                                                                    date_arrived: [sqdate, sqtime],
                                                                    reply_to: reply,
                                                                    replies: [0,0,0,0,0,0,0,0,0],
                                                                    umsgid: 0,
                                                                    ftsc_date: Buffer.from(ftscdate.padEnd(20, 0))
                                                                }
                                                                var msg = {
                                                                    xmsg: xmsg,
                                                                    msg: iconv.convert(req.body.body.replace(/\r\n/g, "\r") + "\r" + origin),
                                                                    ctrl: Buffer.from(ctrl)
                                                                }
                                                                if (typeof(aarr[req.body.area-1]['netmail']) !== "undefined" && aarr[req.body.area-1]['netmail'] == true && typeof(req.body.dest) === "undefined") {
                                                                    cb(new Error('netmail without dest'), null)
                                                                } else if (((typeof(aarr[req.body.area - 1]['netmail']) === undefined) || ((typeof(aarr[req.body.area - 1]['netmail']) !== "undefined" && aarr[req.body.area - 1]['netmail'] == false))) && typeof(req.body.dest) !== "undefined") {
                                                                    cb(new Error('not netmail with dest'), null)
                                                                } else {
                                                                    cb(null, msg) 
                                                                }
                                                            })
                                                        }], (err, msg) => {
                                                            if (!err) {
                                                                squish.SquishWriteMessage(talismanPath + '/' + talismanconfig.paths['message path'] + '/' + aarr[req.body.area-1]['file'], msg, () => {
                                                                    if (mailtype == 1) {
                                                                        touch(talismanPath + "/" + talismanconfig.paths['netmail semaphore'], err => {
                                                                            db.close()
                                                                            res.redirect('/msgbase/' + req.body.conf + '/' + req.body.area)
            
                                                                        });
                                                                    } else if (mailtype == 2) {
                                                                        touch(talismanPath + "/" + talismanconfig.paths['echomail semaphore'], err => {
                                                                            db.close()
                                                                            res.redirect('/msgbase/' + req.body.conf + '/' + req.body.area)
            
                                                                        });
                                                                    } else {
                                                                        db.close()
                                                                        res.redirect('/msgbase/' + req.body.conf + '/' + req.body.area)
                                                                    }
                                                                })
                                                            } else {
                                                                console.log(err.message)
                                                                res.redirect('/msgbase')
                                                                db.close()
                                                            }
                                                        })
                                                    } else {
                                                        res.redirect('/msgbase')
                                                        db.close()
                                                    }
                                                })
                                            })
                                        } else {
                                            res.redirect('/msgbase')
                                            db.close()
                                        }
                                    })
                                }
                            }
                        }
                    }
                }
            })
        })
    } else {
        res.redirect("/login")
    }
})

app.get('/msgbase/post/:cid/:aid', (req, res) => {
    if (req.session.username) {
        var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/users.sqlite3', sqlite3.OPEN_READWRITE, (err) => {
            if (err) {
                console.error(err.message);
                var page = {
                    title: `Post Message - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Unable to open database!'
                }
                res.render('error', page);
                return
            }
        });
        db.serialize(() => {
            db.get("SELECT value FROM details WHERE attrib='seclevel' AND uid = ?", req.session.uid, (err, row) => {
                if (!err && row) {
                    var seclevel = parseInt(row['value'], 10)
                    var msgconfs = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/msgconfs.toml'))
                    var carr = msgconfs['messageconf']
                    var last_read = 0
                    if (req.params.cid <= carr.length && req.params.cid > 0) {
                        
                        if (carr[req.params.cid -1]['sec_level'] <= seclevel) {

                            var msgareas = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/' + carr[req.params.cid-1]['config'] + ".toml"))
                            var aarr = msgareas['messagearea']
                            if (req.params.aid <= aarr.length && req.params.aid > 0) {
                                if (aarr[req.params.aid-1]['write_sec_level'] <= seclevel) {
                                    var isnetmail = false
                                    var iswwiv = false
                                    if (typeof(aarr[req.params.aid-1]['netmail']) !== "undefined" && aarr[req.params.aid-1]['netmail'] === true) {
                                        isnetmail = true
                                        if (typeof(aarr[req.params.aid-1]['wwivnode']) !== "undefined" && aarr[req.params.aid-1]['wwivnode'] !== 0) {
                                            iswwiv = true
                                        }
                                    }

                                    db.get("SELECT value FROM details WHERE attrib='signature_enabled' AND uid=?", req.session.uid, (err, row) => {
                                        if (err || !row || row['value'] === "false") {
                                            var page = {
                                                title: `Post Message - ${talismanconfig.main['system name']}`,
                                                username: req.session.username,
                                                conf: req.params.cid,
                                                area: req.params.aid,
                                                isnetmail: isnetmail,
                                                iswwiv: iswwiv,
                                                signature: ""
                                            }
                                            res.render('postmsg', page)
                                            db.close()
                                        } else {
                                            db.get("SELECT value FROM details WHERE attrib='signature' AND uid=?", req.session.uid, (err, row) => {
                                                var sig = ""
                                                if (!err && row) {
                                                    sig = row['value']
                                                }
                                                var page = {
                                                    title: `Post Message - ${talismanconfig.main['system name']}`,
                                                    username: req.session.username,
                                                    conf: req.params.cid,
                                                    area: req.params.aid,
                                                    isnetmail: isnetmail,
                                                    iswwiv: iswwiv,
                                                    signature: sig
                                                }
                                                res.render('postmsg', page)
                                                db.close()
                                            })
                                        }
                                    })
                                } else {
                                    res.redirect("/msgbase")
                                }
                            } else {
                                res.redirect("/msgbase")
                            }
                        } else {
                            res.redirect("/msgbase")
                        }
                    } else {
                        res.redirect("/msgbase")
                    }
                } else {
                    res.redirect("/msgbase")
                    db.close()
                }
                
            })
        })
    } else {
        res.redirect('/login')
    }
})

app.get('/msgbase/post/:cid/:aid/:mid', (req, res) => {
    if (req.session.username) {
        var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/users.sqlite3', sqlite3.OPEN_READWRITE, (err) => {
            if (err) {
                console.error(err.message);
                var page = {
                    title: `Post Message - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Unable to open database!'
                }
                res.render('error', page);
                return
            }
        });
        db.serialize(() => {
            db.get("SELECT value FROM details WHERE attrib='seclevel' AND uid = ?", req.session.uid, (err, row) => {
                if (!err && row) {
                    var seclevel = parseInt(row['value'], 10)
                    var msgconfs = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/msgconfs.toml'))
                    var carr = msgconfs['messageconf']
                    var last_read = 0
                    if (req.params.cid <= carr.length && req.params.cid > 0) {
                        
                        if (carr[req.params.cid -1]['sec_level'] <= seclevel) {

                            var msgareas = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/' + carr[req.params.cid-1]['config'] + ".toml"))
                            var aarr = msgareas['messagearea']
                            if (req.params.aid <= aarr.length && req.params.aid > 0) {
                                if (aarr[req.params.aid-1]['write_sec_level'] <= seclevel) {
                                    squish.SquishReadMessage(talismanPath + '/' + talismanconfig.paths['message path'] + '/' + aarr[req.params.aid-1]['file'], parseInt(req.params.mid, 10), true, (err, msg) =>{
                                        if (!err) {
                                            var isnetmail = false
                                            var iswwiv = false
                                            if (typeof(aarr[req.params.aid-1]['netmail']) !== "undefined" && aarr[req.params.aid-1]['netmail'] === true) {
                                                isnetmail = true
                                                if (typeof(aarr[req.params.aid-1]['wwivnode']) !== "undefined" && aarr[req.params.aid-1]['wwivnode'] !== 0) {
                                                    iswwiv = true
                                                }                                                
                                            }

                                            db.get("SELECT value FROM details WHERE attrib='signature_enabled' AND uid=?", req.session.uid, (err, row) => {
                                                if (err || !row || row['value'] === "false") {
                                                    var page = {
                                                        title: `Post Message - ${talismanconfig.main['system name']}`,
                                                        username: req.session.username,
                                                        conf: req.params.cid,
                                                        area: req.params.aid,
                                                        reply: req.params.mid,
                                                        subject: msg.header.subject,
                                                        quote: splitbody(msg.body),
                                                        dest: msg.header.origin,
                                                        signature: "", 
                                                        sender: msg.header.from,
                                                        isnetmail: isnetmail,
                                                        iswwiv: iswwiv
                                                    }
                                                    res.render('postmsg', page)    
                                                    db.close()
                                                } else {
                                                    db.get("SELECT value FROM details WHERE attrib='signature' AND uid=?", req.session.uid, (err, row) => {
                                                        var sig = ""
                                                        if (!err && row) {
                                                            sig = row['value']
                                                        }
                                                        var page = {
                                                            title: `Post Message - ${talismanconfig.main['system name']}`,
                                                            username: req.session.username,
                                                            conf: req.params.cid,
                                                            area: req.params.aid,
                                                            reply: req.params.mid,
                                                            subject: msg.header.subject,
                                                            quote: splitbody(msg.body),
                                                            dest: msg.header.origin,
                                                            signature: sig, 
                                                            sender: msg.header.from,
                                                            isnetmail: isnetmail,
                                                            iswwiv: iswwiv
                                                        }
                                                        res.render('postmsg', page)                             
                                                        db.close()
                                                    })
                                                }
                                            })
                                        }
                                    })

                                } else {
                                    res.redirect("/msgbase")
                                }
                            } else {
                                res.redirect("/msgbase")
                            }
                        } else {
                            res.redirect("/msgbase")
                        }
                    } else {
                        res.redirect("/msgbase")
                    }
                } else {
                    res.redirect("/msgbase")
                    db.close()
                }
            
            })
        })
    } else {
        res.redirect('/login')
    }
})

app.get('/msgbase/:cid/:aid/:mid', (req, res) => {
    if (req.session.username) {
        var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/users.sqlite3', sqlite3.OPEN_READWRITE, (err) => {
            if (err) {
                console.error(err.message);
                var page = {
                    title: `Email - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Unable to open database!'
                }
                res.render('error', page);
                return
            }
        });
        db.serialize(() => {
            db.get("SELECT value FROM details WHERE attrib='seclevel' AND uid = ?", req.session.uid, (err, row) => {
                if (!err && row) {
                    var seclevel = parseInt(row['value'], 10)
                    var msgconfs = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/msgconfs.toml'))
                    var carr = msgconfs['messageconf']
                    var last_read = 0
                    if (req.params.cid <= carr.length && req.params.cid > 0) {
                        
                        if (carr[req.params.cid -1]['sec_level'] <= seclevel) {

                            var msgareas = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/' + carr[req.params.cid-1]['config'] + ".toml"))
                            var aarr = msgareas['messagearea']
                            if (req.params.aid <= aarr.length && req.params.aid > 0) {
                                if (aarr[req.params.aid-1]['read_sec_level'] <= seclevel) {
                                    var canwrite = false
                                    if (aarr[req.params.aid-1]['write_sec_level'] <= seclevel) {
                                        canwrite = true
                                    }
                                    async.waterfall([(cb) => {
                                        var realname = req.session.username
                                        db.get("SELECT value FROM details WHERE attrib='fullname' AND uid=?", req.session.uid, (err, row)=>{
                                            if (err) {
                                                console.log(err.message)
                                                cb(err, null)
                                            }
                                            if (row) {
                                                realname = row['value']
                                            }
                                        })
                                        db.get("SELECT mid FROM lastr WHERE msgbase = ? AND uid = ?",talismanconfig.paths['message path'] + '/' + aarr[req.params.aid-1]['file'], req.session.uid, (err, row) => {
                                            if (err) {
                                                console.log(err.message)
                                            }
                                            
                                            if (row) {
                                                last_read = row['mid']
                                            } else {
                                                last_read = -1
                                            }

                                            cb(null, realname)
                                        })
                                    }], (err, realname) => {
                                        squish.SquishReadMessage(talismanPath + '/' + talismanconfig.paths['message path'] + '/' + aarr[req.params.aid-1]['file'], parseInt(req.params.mid, 10), false, (err, msg) => {
                                            if (!err && msg !== null) {
                                                var aka = aarr[req.params.aid-1]['aka']
                                                var oaka = msg.header.origin
                                                if (typeof(aka) != "undefined") {
                                                    if (!aka.includes(".")) {
                                                        aka = aka + ".0"
                                                    }
                                                } else {
						                            if (aarr[req.params.aid-1]['wwivnode'] != null) {
        	                                            aka = aarr[req.params.aid-1]['wwivnode'].toString()
                                                        oaka = "@" + oaka.substring(oaka.indexOf("/") + 1)
                                                        if (oaka.includes(".")) {
                                                            oaka = oaka.substring(0, oaka.indexOf("."));
                                                        }
        			                                }
		                        				}
                                                if (isnetmailarea(aarr[req.params.aid-1]['netmail'])) {
						                            var tmpaka = aka
						                            if (aarr[req.params.aid-1]['wwivnode'] != null) {
							                            tmpaka = "20000:20000/" + aka + ".0";
                        						    }
							
                                                    if (((realname.toUpperCase() === msg.header.to.toUpperCase() && msg.header.destination === tmpaka) ||
                                                    (req.session.username.toUpperCase() === msg.header.to.toUpperCase() && msg.header.destination === tmpaka)) ||
                                                    ((realname.toUpperCase() === msg.header.from.toUpperCase() && msg.header.origin === tmpaka) ||
                                                    (req.session.username.toUpperCase() === msg.header.from.toUpperCase() && msg.header.origin === tmpaka))) {
                                                        if (last_read == -1) {
                                                            db.run("INSERT INTO lastr (mid, uid, msgbase) VALUES(?, ?, ?)", msg.header.id, req.session.uid, talismanconfig.paths['message path'] + '/' + aarr[req.params.aid-1]['file'], (err) => {
                                                                if (err) {
                                                                    console.log(err.message)
                                                                }
                                                            })
                                                        } else if (last_read < msg.header.id) {
                                                            db.run("UPDATE lastr SET mid=? WHERE uid=? and msgbase = ?", msg.header.id, req.session.uid, talismanconfig.paths['message path'] + '/' + aarr[req.params.aid-1]['file'], (err) => {
                                                                if (err) {
                                                                    console.log(err.message)
                                                                }
                                                            })
                                                        }

                                                        squish.SquishNextMessage(talismanPath + '/' + talismanconfig.paths['message path'] + '/' + aarr[req.params.aid-1]['file'], parseInt(req.params.mid, 10), (err, nextmsg) => {
                                                            squish.SquishPrevMessage(talismanPath + '/' + talismanconfig.paths['message path'] + '/' + aarr[req.params.aid-1]['file'], parseInt(req.params.mid, 10), (err, prevmsg) => {
                                                                var page = {
                                                                    title: `${aarr[req.params.aid-1]['name']} - ${talismanconfig.main['system name']}`,
                                                                    username: req.session.username,
                                                                    conf: req.params.cid,
                                                                    area: req.params.aid,
                                                                    confn: carr[req.params.cid-1]['name'],
                                                                    arean: aarr[req.params.aid-1]['name'],
                                                                    data: msg,
                                                                    oaka: oaka,
                                                                    canwrite: canwrite,
                                                                    prev: prevmsg,
                                                                    next: nextmsg
                                                                }
                                                                res.render("msg", page)
                                                            } )
                                                        } )


                                                    } else {
                                                        squish.SquishNextMessage(talismanPath + '/' + talismanconfig.paths['message path'] + '/' + aarr[req.params.aid-1]['file'], parseInt(req.params.mid, 10), (err, nextmsg) => {
                                                            squish.SquishPrevMessage(talismanPath + '/' + talismanconfig.paths['message path'] + '/' + aarr[req.params.aid-1]['file'], parseInt(req.params.mid, 10), (err, prevmsg) => {
                                                                var page = {
                                                                    title: `${aarr[req.params.aid-1]['name']} - ${talismanconfig.main['system name']}`,
                                                                    username: req.session.username,
                                                                    conf: req.params.cid,
                                                                    area: req.params.aid,
                                                                    confn: carr[req.params.cid-1]['name'],
                                                                    arean: aarr[req.params.aid-1]['name'],
                                                                    prev: prevmsg,
                                                                    next: nextmsg
                                                                }
                                                                res.render("privmsg", page)
                                                            })
                                                        })
                                                    }
                                                } else {
                                                    if (last_read == -1) {
                                                        db.run("INSERT INTO lastr (mid, uid, msgbase) VALUES(?, ?, ?)", msg.header.id, req.session.uid, talismanconfig.paths['message path'] + '/' + aarr[req.params.aid-1]['file'], (err) => {
                                                            if (err) {
                                                                console.log(err.message)
                                                            }
                                                        })
                                                    } else if (last_read < msg.header.id) {
                                                        db.run("UPDATE lastr SET mid=? WHERE uid=? and msgbase = ?", msg.header.id, req.session.uid, talismanconfig.paths['message path'] + '/' + aarr[req.params.aid-1]['file'], (err) => {
                                                            if (err) {
                                                                console.log(err.message)
                                                            }
                                                        })
                                                    } 
                                                    squish.SquishNextMessage(talismanPath + '/' + talismanconfig.paths['message path'] + '/' + aarr[req.params.aid-1]['file'], parseInt(req.params.mid, 10), (err, nextmsg) => {
                                                        squish.SquishPrevMessage(talismanPath + '/' + talismanconfig.paths['message path'] + '/' + aarr[req.params.aid-1]['file'], parseInt(req.params.mid, 10), (err, prevmsg) => {

                                                            var page = {
                                                                title: `${aarr[req.params.aid-1]['name']} - ${talismanconfig.main['system name']}`,
                                                                username: req.session.username,
                                                                conf: req.params.cid,
                                                                area: req.params.aid,
                                                                confn: carr[req.params.cid-1]['name'],
                                                                arean: aarr[req.params.aid-1]['name'],
                                                                data: msg,
                                                                oaka: oaka,
                                                                canwrite: canwrite,
                                                                prev: prevmsg,
                                                                next: nextmsg
                                                            }
                                                            res.render("msg", page)
                                                        })
                                                    })
                                                }
                                            } else {
                                                if (err) {
                                                    console.log(err.message)
                                                } 
                                                res.redirect("/msgbase")
                                            }
                                        })
                                    })
                                } else {
                                    res.redirect("/msgbase")
                                }
                            } else {
                                res.redirect("/msgbase")
                            }
                        } else {
                            res.redirect("/msgbase")
                        }
                    } else {
                        res.redirect("/msgbase")
                    }
                } else {
                    res.redirect("/msgbase")
                }
            })
        })
    } else {
        res.redirect('/login')
    }
})

app.get('/msgbase/:cid/:aid', (req, res) => {
    if (req.session.username) {
        var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/users.sqlite3', sqlite3.OPEN_READONLY, (err) => {
            if (err) {
                console.error(err.message);
                var page = {
                    title: `Message Base - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Unable to open database!'
                }
                res.render('error', page);
                return
            }
        });
        db.serialize(() => {
            db.get("SELECT value FROM details WHERE attrib='seclevel' AND uid = ?", req.session.uid, (err, row) => {
                if (!err && row) {
                    var seclevel = parseInt(row['value'], 10)
                    var msgconfs = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/msgconfs.toml'))
                    var carr = msgconfs['messageconf']
                    if (req.params.cid <= carr.length && req.params.cid > 0) {
                        
                        if (carr[req.params.cid -1]['sec_level'] <= seclevel) {

                            var msgareas = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/' + carr[req.params.cid-1]['config'] + ".toml"))
                            var aarr = msgareas['messagearea']
                            if (req.params.aid <= aarr.length && req.params.aid > 0) {
                                var realname = ""
                                if (aarr[req.params.aid-1]['read_sec_level'] <= seclevel) {
                                    var canwrite = false
                                    if (aarr[req.params.aid-1]['write_sec_level'] <= seclevel) {
                                        canwrite = true
                                    }      
                                    async.waterfall([(cb) => {
                                        db.get("SELECT value FROM details WHERE attrib='fullname' AND uid=?", req.session.uid, (err, row)=>{
                                            if (err) {
                                                console.log(err.message)
                                            }
                                            if (row) {
                                                realname = row['value']
                                            }
                                        })
                                        db.get("SELECT mid FROM lastr WHERE uid = ? and msgbase = ?", req.session.uid, talismanconfig.paths['message path'] + "/" + aarr[req.params.aid-1]['file'], (err, row) => {
                                            var lastr
                                            if (err || !row) {
                                                if (err) {
                                                    console.log(err.message)
                                                }
                                                lastr = 0
                                                cb(null, lastr)
                                            } else {
                                                lastr = row['mid']
                                                cb(null, lastr)
                                            }
                                        })

                                    }], (err, lastr) => {
                                        var aka = aarr[req.params.aid-1]['aka']
                                        if (aarr[req.params.aid-1]['wwivnode'] != null) {
                                            aka = aarr[req.params.aid-1]['wwivnode'].toString() 
                                        }
                                        squish.SquishReadHeaders(talismanPath + '/' + talismanconfig.paths['message path'] + '/' + aarr[req.params.aid-1]['file'], parseInt((typeof(req.query.page) === "undefined" ? "0" : req.query.page - 1) * 50, 10), 50, isnetmailarea(aarr[req.params.aid-1]['netmail']), req.session.username,  aka, realname, (err, msgheaders, total) => {
                                            if (!err) {
                                                var page = {
                                                    title: `${aarr[req.params.aid-1]['name']} - ${talismanconfig.main['system name']}`,
                                                    username: req.session.username,
                                                    conf: req.params.cid,
                                                    area: req.params.aid,
                                                    confn: carr[req.params.cid-1]['name'],
                                                    arean: aarr[req.params.aid-1]['name'],
                                                    page: parseInt((typeof(req.query.page) === "undefined" ? "0" : req.query.page - 1), 10),
                                                    total: total,
                                                    last: lastr,
                                                    canwrite: canwrite,
                                                    data: msgheaders
                                                }
                                                res.render("msgheaders", page)
                                                db.close((err) => {
                                                    if (err) {
                                                        console.log(err.message)
                                                    }
                                                })
                                            } else {
                                                var page = {
                                                    title: `${aarr[req.params.aid-1]['name']} - ${talismanconfig.main['system name']}`,
                                                    username: req.session.username,
                                                    error: 'Error reading message base'
                                                }       
                                                res.render("error", page)                                     
                                                db.close((err) => {
                                                    if (err) {
                                                        console.log(err.message)
                                                    }
                                                    cb()
                                                })
                                            }
                                        })
                                    })
                                } else {
                                    res.redirect('/msgbase')
                                    db.close((err) => {
                                        if (err) {
                                            console.log(err.message)
                                        }
                                    })
                                }
                            }
                        } else {
                            res.redirect('/msgbase')
                            db.close((err) => {
                                if (err) {
                                    console.log(err.message)
                                }
                            })
                        }

                    }
                }
            })
        })
    } else {
        res.redirect('/login')
    }
})

app.get('/filebase/:cid/:aid/:fid', (req, res) => {
    if (req.session.username) {
        var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/users.sqlite3', sqlite3.OPEN_READONLY, (err) => {
            if (err) {
                console.error(err.message);
                var page = {
                    title: `Message Base - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Unable to open database!'
                }
                res.render('error', page);
                return
            }
        });
        db.serialize(() => {
            db.get("SELECT value FROM details WHERE attrib='seclevel' AND uid = ?", req.session.uid, (err, row) => {
                if (!err && row) {
                    var seclevel = parseInt(row['value'], 10)
                    var fileconfs = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/fileconfs.toml'))
                    var carr = fileconfs['fileconf']
                    if (req.params.cid <= carr.length && req.params.cid > 0) {
                        
                        if (carr[req.params.cid -1]['sec_level'] <= seclevel) {

                            var fileareas = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/' + carr[req.params.cid-1]['config'] + ".toml"))
                            var aarr = fileareas['filearea']
                            if (req.params.aid <= aarr.length && req.params.aid > 0) {
                                if (aarr[req.params.aid-1]['download_sec_level'] <= seclevel) {
                                    var fdb = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/' + aarr[req.params.aid-1]['database'] + ".sqlite3", sqlite3.OPEN_READWRITE, (err) => {
                                        if (err) {
                                            console.log(err.message)
                                            callback(err, null)
                                        }
                                    })
                                    fdb.serialize(() => {
                                        fdb.get("SELECT filename, dlcount FROM files WHERE id=?", req.params.fid, (err, row) => {
                                            if (!err && row) {
                                                var dlcount = row['dlcount'] + 1
                                                fdb.run("UPDATE files SET dlcount = ? WHERE id = ?", dlcount, req.params.fid)
                                                fs.stat(row['filename'], (err, stats) => {
                                                    if (err) {
                                                        var page = {
                                                            title: `${aarr[req.params.aid-1]['name']} - ${talismanconfig.main['system name']}`,
                                                            username: req.session.username,
                                                            error: 'File unavailable'
                                                        }
                                                        res.render('error', page)
                                                    } else {
                                                        res.download(row['filename'])
                                                    }
                                                })
                                                    
                                                fdb.close((err) => {
                                                    if (err) {
                                                        console.log(err.message)
                                                    }
                                                })    
                                            } else {
                                                if (err) {
                                                    console.log(err.message)
                                                }
                                                res.redirect("/filebase/"+req.params.cid+"/"+req.params.aid)
                                                fdb.close((err) => {
                                                    if (err) {
                                                        console.log(err.message)
                                                    }
                                                }) 
                                            }
                                        })
                                       
                                    })
                                } else {
                                    res.redirect("/filebase")
                                }
                            } else {
                                res.redirect("/filebase")
                            }
                        } else {
                            res.redirect("/filebase")
                        }
                    } else {
                        res.redirect("/filebase")
                    }
                } else {
                    res.redirect("/filebase")
                }
            })
        })
    } else {
        res.redirect('/login')
    }

})

app.get('/filebase/:cid/:aid', (req, res) => {
    if (req.session.username) {
        var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/users.sqlite3', sqlite3.OPEN_READONLY, (err) => {
            if (err) {
                console.error(err.message);
                var page = {
                    title: `Message Base - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Unable to open database!'
                }
                res.render('error', page);
                return
            }
        });
        db.serialize(() => {
            db.get("SELECT value FROM details WHERE attrib='seclevel' AND uid = ?", req.session.uid, (err, row) => {
                if (!err && row) {
                    var seclevel = parseInt(row['value'], 10)
                    var fileconfs = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/fileconfs.toml'))
                    var carr = fileconfs['fileconf']
                    if (req.params.cid <= carr.length && req.params.cid > 0) {
                        
                        if (carr[req.params.cid -1]['sec_level'] <= seclevel) {

                            var fileareas = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/' + carr[req.params.cid-1]['config'] + ".toml"))
                            var aarr = fileareas['filearea']
                            if (req.params.aid <= aarr.length && req.params.aid > 0) {
                                if (aarr[req.params.aid-1]['download_sec_level'] <= seclevel) {
                                    async.waterfall([(callback) => {
                                        var fdb = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/' + aarr[req.params.aid-1]['database'] + ".sqlite3", sqlite3.OPEN_READONLY, (err) => {
                                            if (err) {
                                                console.log(err.message)
                                                callback(err, null)
                                            }
                                        })
                                        fdb.serialize(() => {
                                            var files = []
                                            fdb.all("SELECT id, filename, filesize, dlcount, uldate, ulname, descr FROM files ORDER BY filename", (err, rows) => {
                                                var iconv = new Iconv("CP437", "ASCII//TRANSLIT//IGNORE")
                                                rows.forEach((row) => {
                                                    if (!err) {
                                                        var sz = 0

                                                        if (row['filesize'] > 1024 * 1024) {
                                                            sz = (row['filesize'] / (1024 * 1024)).toFixed(2) + "Mb"
                                                        } else if (row['filesize'] > 1024) {
                                                            sz = (row['filesize'] / 1024).toFixed(2) + "Kb"
                                                        } else {
                                                            sz = row['filesize'].toString() + "b"
                                                        }

                                                        var file = {
                                                            id: row['id'],
                                                            name: path.basename(row['filename']),
                                                            size: sz,
                                                            dlcount: row['dlcount'],
                                                            uldate: row['uldate'],
                                                            ulname: row['ulname'],
                                                            descr: iconv.convert(ansi.convert(row['descr'].replace(/\r\n/g, "\r").replace(/\n/g, "\r"), 46)).toString()
                                                        }

                                                        files.push(file)
                                                    } else {
                                                        console.log(err.message)
                                                    }
                                                })
                                                fdb.close((err) => {
                                                    if (err) {
                                                        console.log(err.message)
                                                    }
                                                })
                                                callback(null, files)                                                
                                            })
                                        })
                                    }], (err, files) => {
                                        if (err) {
                                            console.log(err.message)
                                            var page = {
                                                title: `${aarr[req.params.aid-1]['name']} - ${talismanconfig.main['system name']}`,
                                                username: req.session.username,
                                                error: 'Error reading message base'
                                            } 
                                            res.render('error', page)
                                        } else {
                                            var page = {
                                                title: `${aarr[req.params.aid-1]['name']} - ${talismanconfig.main['system name']}`,
                                                username: req.session.username,
                                                conf: req.params.cid,
                                                area: req.params.aid,
                                                confn: carr[req.params.cid-1]['name'],
                                                arean: aarr[req.params.aid-1]['name'],
                                                data: files
                                            }                                            
 
                                            res.render('filelist', page)                                            
                                        }
                                    });                                    
                                } else {
                                    res.redirect('/filebase')
                                    db.close((err) => {
                                        if (err) {
                                            console.log(err.message)
                                        }
                                    })
                                }
                            }
                        } else {
                            res.redirect('/filebase')
                            db.close((err) => {
                                if (err) {
                                    console.log(err.message)
                                }
                            })
                        }

                    }
                }
            })
        })        
    } else {
        res.redirect('/login')
    }
})

app.get('/filebase', (req, res) => {
    if (req.session.username) {
        if (req.session.username) {
            var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/users.sqlite3', sqlite3.OPEN_READONLY, (err) => {
                if (err) {
                    console.error(err.message);
                    var page = {
                        title: `File Bases - ${talismanconfig.main['system name']}`,
                        username: req.session.username,
                        error: 'Unable to open database!'
                    }
                    res.render('error', page);
                    return
                }
            });
            db.serialize(() => {
                db.get("SELECT value FROM details WHERE attrib='seclevel' AND uid = ?", req.session.uid, (err, row) => {
                    if (!err && row) {
                        var seclevel = parseInt(row['value'], 10)
                        var fileconfs = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/fileconfs.toml'))
    
                        var confs = []
                        var ccount = 0
                        async.forEachOfSeries(fileconfs['fileconf'], (a, key, cbouter2) => {
                            ccount++
                            var areas = []
                            if (a['sec_level'] <= seclevel) {
                                var acount = 0;
                                var fileareas = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + "/" + a['config'] + '.toml'))
    
                                async.forEachOfSeries(fileareas['filearea'], (b, key, cbouter) => {
                                    acount++
                                    
                                    if (b['download_sec_level'] <= seclevel) {
                                        async.waterfall([(callback) => {
                                            var count
                                            var fdb = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/' + b['database'] + ".sqlite3", sqlite3.OPEN_READONLY, (err) => {
                                                if (err) {
                                                    console.log(err.message)
                                                    callback(err, null)
                                                }
                                            })
                                            fdb.serialize(() => {
                                                fdb.get("SELECT COUNT(*) AS count FROM files", (err, row) => {
                                                    if (!err && row) {
                                                        count = row['count']
                                                    } else {
                                                        count = 0
                                                    }
                                                    fdb.close((err) => {
                                                        if (err) {
                                                            console.log(err.message)
                                                        }
                                                    })
                                                    callback(null, count)        
                                                })
                                            })

                                        }], (err, count) => {
                                            if (err) {
                                                console.log(err.message)
                                                cbouter(null)
                                            } else {
                                                var area = {
                                                    name: b['name'],
                                                    index: acount,
                                                    files: count
                                                }
                                                areas.push(area)
                                                cbouter(null)
                                            }
                                        });
    
                                    } else {
                                        cbouter(null)
                                    }
                                    
                                }, (err) => {
                                    if (!err) {
                                        var conf = {
                                            name: a['name'],
                                            areas: areas,
                                            index: ccount
                                        }
                                        confs.push(conf)
                                        cbouter2(null)                                       
                                    } else {
                                        console.log(err.message)
                                        cbouter2(null)
                                    }
                                })
                            }
                        }, (err) => {
                            var page = {
                                title: `File Bases - ${talismanconfig.main['system name']}`,
                                username: req.session.username,
                                confs: confs
                            }                    
                            res.render('filebase', page)
                            db.close((err) => {
                                if (err) {
                                    console.log(err.message)
                                }
                            })
                        })
    
                    } else {
                        console.log(err.msg)
                        db.close((err) => {
                            if (err) {
                                console.log(err.message)
                            }
                        })
                    }                    
                })
            })
        }
    } else {
        res.redirect('/login')
    }
})

app.get('/msgbase', (req, res) => {
    if (req.session.username) {
        var db = new sqlite3.Database(talismanPath + '/' + talismanconfig.paths['data path'] + '/users.sqlite3', sqlite3.OPEN_READONLY, (err) => {
            if (err) {
                console.error(err.message);
                var page = {
                    title: `Message Bases - ${talismanconfig.main['system name']}`,
                    username: req.session.username,
                    error: 'Unable to open database!'
                }
                res.render('error', page);
                return
            }
        });
        db.serialize(() => {
            db.get("SELECT value FROM details WHERE attrib='seclevel' AND uid = ?", req.session.uid, (err, row) => {
                if (!err && row) {
                    var seclevel = parseInt(row['value'], 10)
                    var msgconfs = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + '/msgconfs.toml'))

                    var confs = []
                    var ccount = 0
                    async.forEachOfSeries(msgconfs['messageconf'], (a, key, cbouter2) => {
                        ccount++
                        var areas = []
                        if (a['sec_level'] <= seclevel) {
                            var acount = 0;
                            var msgareas = TOML.parse(fs.readFileSync(talismanPath + '/' + talismanconfig.paths['data path'] + "/" + a['config'] + '.toml'))

                            async.forEachOfSeries(msgareas['messagearea'], (b, key, cbouter) => {
                                acount++
                                
                                if (b['read_sec_level'] <= seclevel) {
                                    async.waterfall([(callback) => {
                                        squish.SquishReadSqBaseHdr(talismanPath + '/' + talismanconfig.paths['message path'] + '/' + b['file'], (err, sqhdr) => {
                                            callback(err, sqhdr)
                                        })
                                    }, (sqhdr, callback) => {
                                        if (sqhdr != null) {
                                            var area = {
                                                name: b['name'],
                                                index: acount,
                                                total: sqhdr.num_msg
                                            };
                                        } else {
                                            var area = {
                                                name: b['name'],
                                                index: acount,
                                                total: 0
                                            };
                                        }
                                        callback(null, area)
                                    }, (area, callback) => {
                                        db.get("SELECT mid FROM lastr WHERE uid = ? and msgbase = ?", req.session.uid, talismanconfig.paths['message path'] + "/" + b['file'], (err, row) => {
                                            if (err || !row) {
                                                if (err) {
                                                    console.log(err.message)
                                                }
                                                area['read'] = 0
                                                callback(null, area)
                                            } else {
                                                squish.SquishUMSGID2Offset(talismanPath + '/' + talismanconfig.paths['message path'] + '/' + b['file'], row['mid'], false, (err, n) => {
                                                    if (!err) {
                                                        area['read'] = n
                                                    } else {
                                                        console.log(err.message)
                                                        area['read'] = 0
                                                    }
                                                    callback(null, area)
                                                })
                                            }
                                        })
                                    }], (err, area) => {
                                        if (err) {
                                            console.log(err.message)
                                            cbouter(null)
                                        } else {
                                            areas.push(area)
                                            cbouter(null)
                                        }
                                    });

                                } else {
                                    cbouter(null)
                                }
                                
                            }, (err) => {
                                if (!err) {
                                    var conf = {
                                        name: a['name'],
                                        areas: areas,
                                        index: ccount
                                    }
                                    confs.push(conf)
                                    cbouter2(null)                                       
                                } else {
                                    console.log(err.message)
                                    cbouter2(null)
                                }
                            })
                        }
                    }, (err) => {
                        var page = {
                            title: `Message Bases - ${talismanconfig.main['system name']}`,
                            username: req.session.username,
                            confs: confs
                        }                    
                        res.render('msgbase', page)
                        db.close((err) => {
                            if (err) {
                                console.log(err.message)
                            }
                        })                        
                    })

                } else {
                    console.log(err.message)
                    db.close((err) => {
                        if (err) {
                            console.log(err.message)
                        }
                    })
                }
            })
        })
    } else {
        res.redirect('/login')
    }
})

app.get('*', (req, res) => {
    res.status(404).render('404', {title: 'Resource not found!', username: req.session.username});
});


app.listen(port, () => {
    console.log(`Noddy listening at http://localhost:${port}`)
})

