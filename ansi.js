function convert(data, width=0) {
    var line_at = 0
    var col_at = 0
    var param_count = 0;
    var params = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    var save_col = 0
    var save_line = 0
    var fg_color = 7
    var bg_color = 0
    var bold = false
    var fakescreen = []
    var lines = 0
    var cols = 0

    for (var i = 0; i < data.length; i++) {
        if (data[i] === '\r') {
            line_at++
            if (line_at > lines) {
                lines = line_at;
            }
            col_at = 0
        } else if(i < data.length - 2 && data[i] === '|' && ("1234567890".includes(data[i+1]) && ("1234567890").includes(data[i+2]))) {
            i+=2
            continue
        } else if (data[i] === '\x1b') {
            i++
            if (data[i] !== '[') {
                i--
                continue
            } else {
                param_count = 0
                while (i < data.length && !("ABCDEFGHIGJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".includes(data[i]))) {
                    if (data[i] === ';') {
                        param_count++
                    } else if (data[i] >= '0' && data[i] <= '9') {
                        if (param_count == 0) {
                            param_count = 1
                            for (var j = 0; j < 9; j++) {
                                params[j] = 0
                            }
                        }
                    
                        params[param_count - 1] = params[param_count - 1] * 10 + (parseInt(data[i], 10));
                    }
                    i++
                }
            }
            switch(data[i]) {
                case 'A':
                    if(param_count > 0) {
                        line_at -= params[0]
                    } else {
                        line_at--
                    }
                    if (line_at < 0) {
                        line_at = 0
                    }
                    break;
                case 'B':
                    if (param_count > 0) {
                        line_at += params[0]
                    } else {
                        line_at++
                    }
                    if (line_at > lines) {
                        lines = line_at
                    }
                    break
                case 'C':
                    if (param_count > 0) {
                        col_at += params[0]
                    } else {
                        col_at++
                    }
                    if (width > 0) {
                        if (col_at > width) {
                            col_at = width
                        }
                    } else {
                        if (col_at > cols) {
                            cols = col_at
                        }
                    }
                    break
                case 'D':
                    if (param_count > 0) {
                        col_at -= params[0]
                    } else {
                        col_at--
                    }
                    if (col_at < 0) {
                        col_at = 0
                    }
                    break;
                case 'H':
                case 'f':
                    if (param_count > 1) {
                        params[0]--
                        params[1]--
                    }
                    line_at = params[0]
                    col_at = params[1]

                    if (line_at > lines) {
                        lines = line_at
                    }
                    if (width > 0) {
                        if (col_at > width) {
                            col_at = width
                        }
                    } else {
                        if (col_at > cols) {
                            cols = col_at
                        }
                    }
                    if (line_at < 0) {
                        line_at = 0
                    }
                    if (col_at < 0) {
                        col_at = 0
                    }
                    break
                case 'u':
                    col_at = save_col
                    line_at = save_line
                    break;
                case 's':
                    save_col = col_at
                    save_line = line_at
                    break
                default:
                    break
            }
        } else if (data[i] !== '\n') {
            col_at++
            if (width > 0) {
                if (col_at >= width) {
                    col_at = 0
                    line_at++
                    if (line_at > lines) {
                        lines = line_at
                    }
                }
            } else {
                if (col_at > cols) {
                    cols = col_at
                }
            }
        }
    }


    for (var y = 0; y <= lines; y++) {
        var row = []

        for (var x = 0; x <= (width > 0 ? width : cols); x++) {
            var ch = {
                c: ' ',
                bold: false,
                fg: 7,
                bg: 0
        
            }        
            row.push(ch)
        }
        fakescreen.push(row)
    }
    line_at = 0
    col_at = 0
    save_line = 0
    save_col = 0

    for (var i = 0; i < data.length; i++) {
        if (data[i] == '\r') {
            line_at++
            col_at = 0
        } else if(i < data.length - 2 && data[i] === '|' && ("1234567890".includes(data[i+1]) && ("1234567890").includes(data[i+2]))) {
            var c = parseInt(data[i + 1], 10) * 10 + parseInt(data[i + 2], 10)
            i += 2
            switch (c) {
                case 0:
                    bold = false
                    fg_color = 0
                    break
                case 1:
                    bold = false
                    fg_color = 4
                    break                    
                case 2:
                    bold = false
                    fg_color = 2
                    break
                case 3:
                    bold = false
                    fg_color = 6
                    break
                case 4:
                    bold = false
                    fg_color = 1
                    break
                case 5:
                    bold = false
                    fg_color = 5
                    break
                    bold = false
                case 6:
                    bold = false
                    fg_color = 3
                    break
                case 7:
                    bold = false
                    fg_color = 7
                    break
                case 8:
                    bold = true
                    fg_color = 0
                    break
                case 9:
                    bold = true
                    fg_color = 4
                    break
                case 10:
                    bold = true
                    fg_color = 2
                    break
                case 11:
                    bold = true
                    fg_color = 6
                    break
                case 12:
                    bold = true
                    fg_color = 1
                    break
                case 13:
                    bold = true
                    fg_color = 5
                    break
                case 14:
                    bold = true
                    fg_color = 3
                    break
                case 15:
                    bold = true
                    fg_color = 7
                    break
                case 16:
                    bg_color = 0
                    break
                case 17:
                    bg_color = 4
                    break
                case 18:
                    bg_color = 2
                    break
                case 19:
                    bg_color = 6
                    break
                case 20:
                    bg_color = 1
                    break
                case 21:
                    bg_color = 5
                    break
                case 22:
                    bg_color = 3
                    break
                case 23:
                    bg_color = 7
                    break
                    
            }
        } else if (data[i] == '\x1b') {
            i++
            if (data[i] != '[') {
                i--
                continue
            } else {
                param_count = 0
                while (i < data.length && !("ABCDEFGHIGJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".includes(data[i]))) {
                    if (data[i] == ';') {
                        param_count++
                    } else if (data[i] >= '0' && data[i] <= '9') {
                        if (param_count == 0) {
                            param_count = 1
                            for (var j = 0; j < 9; j++) {
                                params[j] = 0
                            }
                        }
                    
                        params[param_count - 1] = params[param_count - 1] * 10 + (parseInt(data[i], 10));
                    }

                    i++
                }
                
            }
            switch(data[i]) {
                case 'A':
                    if(param_count > 0) {
                        line_at -= params[0]
                    } else {
                        line_at--
                    }
                    if (line_at < 0) {
                        line_at = 0
                    }
                    break;
                case 'B':
                    if (param_count > 0) {
                        line_at += params[0]
                    } else {
                        line_at++
                    }
                    if (line_at > lines) {
                        line_at = lines
                    }
                    break
                case 'C':
                    if (param_count > 0) {
                        col_at += params[0]
                    } else {
                        col_at++
                    }
                    if (width > 0) {
                       if (col_at > width) {
                            col_at = width
                        }
                    } else {
                        if (col_at > cols) {
                            col_at = cols
                        }
                    }
                    break
                case 'D':
                    if (param_count > 0) {
                        col_at -= params[0]
                    } else {
                        col_at--
                    }
                    if (col_at < 0) {
                        col_at = 0
                    }
                    break;
                case 'H':
                case 'f':
                    if (param_count > 1) {
                        params[0]--
                        params[1]--
                    }
                    line_at = params[0]
                    col_at = params[1]

                    if (line_at > lines) {
                        line_at = lines
                    }

                    if (col_at > width) {
                        col_at = width
                    }
                    if (line_at < 0) {
                        line_at = 0
                    }
                    if (col_at < 0) {
                        col_at = 0
                    }
                    break
                case 'u':
                    col_at = save_col
                    line_at = save_line
                    break;
                case 's':
                    save_col = col_at
                    save_line = line_at
                    break
                case 'm':
                    for (var z = 0; z < param_count; z++) {
                        if (params[z] == 0) {
                            bold = false
                            fg_color = 7
                            bg_color = 0
                        } else if (params[z] == 1) {
                            bold = true
                        } else if (params[z] == 2) {
                            bold = false
                        } else if (params[z] >= 30 && params[z] <= 37) {
                            fg_color = params[z] - 30
                        } else if (params[z] >= 40 && params[z] <= 47) {
                            bg_color = params[z] - 40
                        }
                    }
                    break
                default:
                    break
            }
        } else if (data[i] !== '\n') {
            
            fakescreen[line_at][col_at].c = data[i]
            fakescreen[line_at][col_at].bold = bold
            fakescreen[line_at][col_at].fg = fg_color
            fakescreen[line_at][col_at].bg = bg_color
            col_at++
            if (col_at >= (width > 0 ? width : cols + 1)) {
                col_at = 0
                line_at++
                if (line_at > lines) {
                    line_at = lines
                }
            }
        }
    }

    for (var i = 0; i < lines; i++) {
        for (var j = (width > 0 ? width : cols) -1; j >= 0; j--) {
            if (fakescreen[i][j].c === ' ') {
                fakescreen[i][j].c = '\0';
            } else {
                break;
            }
        }
    }
    fg_color = 7;
    bg_color = 0;
    bold = false;
  
    var ansi = ""
    var span = false;

    var colors = [
        "#000000",
        "#880000",
        "#008800",
        "#888800",
        "#000088",
        "#880088",
        "#008888",
        "#BBBBBB"
    ]

    var colors_bold = [
        "#888888",
        "#ff8888",
        "#88ff88",
        "#ffff88",
        "#8888ff",
        "#ff88ff",
        "#88ffff",
        "#ffffff"
    ]

    for (var y = 0; y < lines; y++) {
        var lastspace = true
        for (var x = 0; x < (width > 0 ? width : cols); x++) {
            if (fakescreen[y][x].c == '\0') {
                break
            }


            if (fakescreen[y][x].bold != bold || fakescreen[y][x].fg != fg_color || fakescreen[y][x].bg != bg_color) {
                bold = fakescreen[y][x].bold
                fg_color = fakescreen[y][x].fg
                bg_color = fakescreen[y][x].bg
                if (span) {
                    ansi += "</span>"
                }
                
                span = true

                if (bold) {
                    ansi += "<span style=\"color:" + colors_bold[fg_color] + ";background-color:" + colors[bg_color] + ";\">";
                } else {
                    ansi += "<span style=\"color:" + colors[fg_color] + ";background-color:" + colors[bg_color] + ";\">";
                }
                lastspace = true
            }
            
            if (fakescreen[y][x].c == ' ') {
                if (lastspace) {
                    ansi += "&nbsp;"
                } else {
                    ansi += ' '
                }
                lastspace = true
            } else {
                lastspace = false
            }
            
            
            if (fakescreen[y][x].c == '>') {
                ansi += "&gt;"
            } else if (fakescreen[y][x].c == '_') {
                ansi += "&lowbar;"
            } else if (fakescreen[y][x].c == '<') {
                ansi += "&lt;"
            } else if (fakescreen[y][x].c != ' ') {
                ansi += fakescreen[y][x].c
            }
        }

        ansi += '<br />'
    }

    if (span) {
        ansi += "</span>"
    }
    ansi += '<br />'
    

    return ansi
}

function strip(str) {
   var carr = [...str]
   var ret = ""
   var state = 0;
   for (var i = 0; i < carr.length; i++) {
       if (carr[i] == '\x1b') {
           state = 1;
       } else if (state == 0) {
           ret = ret + carr[i]
       } else {
          if ("ABCDEFGHIGJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".includes(carr[i])) {
              state = 0;
          }
       } 
   }
   return ret
}

function parse_pipe_codes(str) {
    var carr = [...str]
    var fg = 7
    var bg = 16

    var colours = [
        "#000000",
        "#000088",
        "#008800",
        "#008888",
        "#880000",
        "#880088",
        "#888800",
        "#BBBBBB",
        "#888888",
        "#8888FF",
        "#88FF88",
        "#88FFFF",
        "#FF8888",
        "#FF88FF",
        "#FFFF88",
        "#FFFFFF"
    ]

    var ret = "<span style=\"color: #BBBBBBB;background-color: #000000\">"
    for (var i = 0; i < carr.length; i++) {
        if (carr[i] == '|' && i < carr.length - 2) {
            i++
            var colour = 0
            if ("0123456789".includes(carr[i])) {
                colour = parseInt(carr[i], 10)

            }
            i++
            if ("0123456789".includes(carr[i])) {
                colour = colour * 10 + parseInt(carr[i], 10)
            }

            if (colour > 15) {
                bg = colour - 16
            } else {
                fg = colour
            }
            ret += "<span style=\"color: " + colours[fg] + "; background-color: " + colours[bg] + ";\">"
            continue
        }
        ret += carr[i]
    }
    ret += "</span>"

    return ret
}

exports.strip = strip
exports.convert = convert
exports.parse_pipe_codes = parse_pipe_codes
